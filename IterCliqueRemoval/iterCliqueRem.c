#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>/*for gettimeofday*/


char mfile[2048] = {'\0'};
int iters=0;
typedef struct {
  int u;
  int v;
} edge;

typedef struct {
  int to;
  int id;
} edgeid;

void help() {
  printf("usage: ex2\n");
  printf("\t -f in filename\n");
    printf("\t -i num of iterations\n");


}
int cmpfunc (const void * a, const void * b) //what is it returning?
{
   int* arg1 = (int*) a;
   int* arg2 = (int*) b;
   if( *arg1 < *arg2 ) return -1;
   else if( *arg1 == *arg2 ) return 0;
   else return 1;
 }


double u_seconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
    
}

int parse_args(int argc, char *argv[]) {
  int opt;
  int mfile_provided = 0;
  int itersgiven=0;
  while((opt = getopt(argc, argv, "f:i:")) != -1){
    switch(opt) {
    case 'f':
      strcpy(mfile, optarg);
      mfile_provided = 1;
      break;
    case 'i':
      iters = atoi(optarg);
      itersgiven=1;
      break;  
    }
  }
    
 
  if(!mfile_provided){
    printf("You need to provide a file!!!");
    help();
    return 0;
  }
  if (itersgiven==0){
    printf("You need to give num of iterations");
    help();
    return 0;
  }
  return 1;
}
void readfile(char *fname, edge **edges,int *n,int *m){
  FILE *in_file=fopen(fname, "r");
  int tn,tm, u,v,ei;
  fscanf(in_file, "%d", &tn);
  fscanf(in_file, "%d", &tm); 
  //find space
  *edges = (edge *)malloc(tm * sizeof(edge)); 

  for (ei=0; ei<tm;ei++) {
    fscanf(in_file, "%d %d",&u,&v);
    if  (v>=tn) tn=v+1;
    if (u>=tn) tn=u+1;
    (*edges)[ei].u=u;
    (*edges)[ei].v=v;
  }
  *n=tn;
  *m=tm;
  fclose(in_file);
}
void initdegrees(int *d, edge *edges,int m){
  for (int ei=0;ei<m;++ei){
      d[edges[ei].u]++; 
      d[edges[ei].v]++;
  }
}
void create_csa(int *d,int n, int m,int *xids, int *eids, int *ids, edge *edges){
    xids[0]=0;
    for (int i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
    }
    for (int ei=0;ei<m;++ei){
      int eu=edges[ei].u;
      int ev=edges[ei].v;
      ids[xids[eu]++]=ev;
      ids[xids[ev]++]=eu;
    }
    xids[0]=0;
    for (int i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
        eids[i-1]=xids[i]-1;
    }
}



int binSearchClique(int target,int *array, int l,int r){
    int pos=-1;
    while (l<r){
      int m = (l+r)/2;
      if (array[m]==target) {
        pos=m;
        break;
      }
      else if (array[m] > target){
        r=m;
      } else{
        l=m+1;
      }
    }
    return pos;
}
void cliqueRemoval(int *cliqueSet,int endcl,int *xids,int *eids,int *ids,int *d){ //removes clique from the graph
  qsort(cliqueSet,endcl,sizeof(int),cmpfunc); //sort for binary search queries
  for (int i=0;i<endcl;++i){
		int x=cliqueSet[i];
		for (int j=xids[x];j<=eids[x];++j){
			int z=ids[j];
			int pos=binSearchClique(z,cliqueSet,0,endcl); //see if endpoint is in the clique or not
      //printf("edge (%d,%d) on clique: %d \n",x,z,pos);
			if (pos != -1 ){ //if  in the clique, remove by swapping with last (and update ends array)
				d[x]--;
				ids[j]=ids[eids[x]];
				j--;
				eids[x]--;
			}
		}
	}
}
void cliqueFinderMethod1(int *xids,int *eids,int *ids, int *d,int n,int *cliqueSet,int *endcl){
  /*
  *Adapted by: 
  *   Fast Algorithms for the Maximum Clique Problem on Massive Sparse Graphs
  *   Bharath Pattabiraman, Md. Mostofa Ali Patwary,Assefaw H. Gebremedhin,Wei-keng Liao, and Alok Choudhary
  *   -----
  *   https://arxiv.org/pdf/1209.5818.pdf
  */
  int *tempclique=(int*)malloc(n*sizeof(int));
  unsigned long *round=(unsigned long*)malloc(n*sizeof(unsigned long));

	int currmax=0;
  int maxd=-1;
  unsigned long r=0;
  for (int i=0;i<n;++i){ 
    round[i]=0;
  }
	for (int i=0;i<n;++i){ //iterate over vertices
		if (d[i] >= currmax){ //prune 1
        r++;
        int endtemp=0;
        int cands=0;
			  for (int j=xids[i];j<=eids[i];++j){ //iterate over i's vertices
          int z=ids[j];
          if (d[z] >= currmax){ //prune 3
              round[z]=r;  //signal that z is in i's neighborhood  (i.e., e U)
              cands++;
              if (maxd==-1) //also find vertex of max. degree in this iteration
                maxd=z;
              else{
                if (d[maxd] < d[z])
                  maxd=z;
              }
          }
       }
       tempclique[endtemp++]=i; //add i to clique
       while (cands!=0){ //while there exist candidate vertices!
          int x=maxd;
          tempclique[endtemp++]=x;   //add to the clique the max. degree one 
     //     printf("%d : adding %d \n",i,x);
          cands=0; //set cands to 0  (to recalculate!)
          maxd=-1; 
          r++; //update r to update U  implicitly
          for (int j=xids[x];j<=eids[x];++j){ //iterate over x's neighbors 
              int w=ids[j];
       //     printf("%d: now on %d \n",i,w);
              if (d[w]>=currmax && round[w]==r-1){ //prune 5 and check if part of U
                  round[w]++; //update condition for U
                  cands++; //
                  if (maxd==-1) //also update max. degree
                    maxd=w;
                  else{
                    if (d[maxd]  < d[w])
                      maxd=w;
                  } 

              }       
          }
       } 
       if (currmax  < endtemp) { //if the clique is larger, than the largest found
          for (int i=0;i<endtemp;++i){ 
            cliqueSet[i]=tempclique[i]; //replace!
          }
          currmax=endtemp;
      }
     }
	}
  *endcl=currmax;
  free(tempclique);
  free(round);
} 
void cliqueFinderMethod2(int *xids,int *eids,int *ids, int *d,int n){
  //maybe nother day
}
void printgraph(int *xids,int *eids,int *ids,int *d,int n){
  for (int i=0;i<n;++i){
    printf("%d (%d): ",i,d[i]);
    for (int j=xids[i];j<=eids[i];++j){
      printf("%d ",ids[j]);
    }
    printf("\n");
  }
}
void iterativeCliqueFind(int iters,int *xids,int *eids,int *ids,int *d,int n){
  int *cliqueSet=(int*)malloc(n*sizeof(int));
  int endcl=0;
  for (int i=0;i<iters;++i){
    double t0=u_seconds();
      cliqueFinderMethod1(xids,eids,ids,d,n,cliqueSet,&endcl);
    double t1=u_seconds();
    printf("Iteration %d : %d  took %.2f seconds ",i,endcl,t1-t0);
    if (endcl==1){
      printf("EMPTY GRAPH \n");
      break;
    }
    /*for (int z=0;z<endcl;++z){
      printf("%d ",cliqueSet[z]);
    }*/
    printf("\n");
    t0=u_seconds();
      cliqueRemoval(cliqueSet,endcl,xids,eids,ids,d);
    t1=u_seconds();
    printf("Clique removal took %.2f seconds \n",t1-t0);
  }
  free(cliqueSet);
}
int main(int argc, char *argv[]) {
    if(!parse_args(argc,argv)){
    return -1;
  } 
  printf("Running on %s \n",mfile);

    int n,m;
    int *d;
    edge *edges;
    double t0= u_seconds();
        readfile(mfile,&edges,&n,&m);
    double t1= u_seconds();
    printf("Reading input  took %.2f seconds \n",t1-t0);
    int sum=0;
    d=(int*)calloc(n,sizeof(int));
    initdegrees(d,edges,m);
    int *xids=(int*)malloc((n+1)*sizeof(int));
    int *eids=(int*)malloc((n)*sizeof(int));
    int *ids=(int*)malloc(2*m*sizeof(int));
    t0= u_seconds();
        create_csa(d,n,m,xids,eids,ids,edges);
    t1=u_seconds();  
        printf("Creating csv took %.2f seconds \n",t1-t0);

    t0= u_seconds();
    iterativeCliqueFind(iters,xids,eids,ids,d,n);
   t1=u_seconds();  
       printf("Iterative Clique took %.2f seconds\n",t1-t0); 
    printf("------- \n");
    free(edges);

    free(d);
    free(xids);
    free(ids);
    free(eids);
  return 0;
}

