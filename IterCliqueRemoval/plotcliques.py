import numpy as np
import sys
import matplotlib.pyplot as plt

DATA=sys.argv[1]

#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
#plt.rcParams['text.usetex'] = True

def parse(inputfile):
	SIZE=[]
	TIME=[]
	f=open(inputfile,"r")
	for line in f:
		line=line.split()
		if len(line)>1 and line[0]=="Iteration":
			s=int(line[3])
			t=float(line[5])
			SIZE.append(s)
			TIME.append(t)
	return SIZE,TIME

SIZE,TIME=parse(DATA)

plt.plot(range(1,len(SIZE)+1),SIZE,".")
plt.title(DATA+" size",fontsize=22)
plt.xlabel("Number of iterations",fontsize=22)
plt.ylabel("Size of the cliques",fontsize=22)
#plt.xlim([1,2100])
#plt.tight_layout()
plt.savefig(DATA+"_size.pdf")
plt.show()

plt.plot(range(1,len(SIZE)+1),TIME,".")
plt.title(DATA+" time",fontsize=22)
plt.xlabel("Number of iterations",fontsize=22)
plt.ylabel("Time to find clique (seconds)",fontsize=22)
#plt.xlim([1,2100])
#plt.tight_layout()
plt.savefig(DATA+"_time.pdf")
plt.show()

