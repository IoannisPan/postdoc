#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>/*for gettimeofday*/
#include <omp.h>
#include <atomic>
using namespace std;
#define NLINKS 100000000 //maximum number of edges for memory allocation, will increase if needed
unsigned long long maxlimit=ULLONG_MAX;
char mfile[2048] = {'\0'};
char ofile[2048] = {'\0'};
int numthreads;
unsigned long iters=0;
typedef struct {
  unsigned long  u;
  unsigned long  v;
} edge;

typedef struct {
  unsigned long  v;
  unsigned long  d;
} vid;

void help() {
  printf("usage: ex2\n");
  printf("\t -f in filename\n");
  printf("\t -o out filename \n");
  printf("\t -i num of iterations\n");


}
int cmpfunc (const void * a, const void * b) //what is it returning?
{
   unsigned long * arg1 = (unsigned long *) a;
   unsigned long * arg2 = (unsigned long *) b;
   if( *arg1 < *arg2 ) return -1;
   else if( *arg1 == *arg2 ) return 0;
   else return 1;
 }
 int cmpfunc1 (const void * a, const void * b) //what is it returning?
{
   vid *arg1 = (vid*) a;
   vid *arg2 = (vid*) b;
   if( arg1->d < arg2->d ) return -1;
   else if( arg1->d == arg2->d ) return 0;
   else return 1;
 }


double u_seconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
    
}
int parse_args(int argc, char *argv[]) {
  int opt;
  int mfile_provided = 0;
   int thread_provided=0;
  int itersgiven=0;
  while((opt = getopt(argc, argv, "f:c:i:")) != -1){
    switch(opt) {
    case 'f':
      strcpy(mfile, optarg);
      mfile_provided = 1;
      break;
       case 'c':
       numthreads = atoi(optarg);
       thread_provided=1;
      break;
    case 'i':
      iters = atoi(optarg);
      itersgiven=1;
      break;  
    }
  }
    
 
  if(!mfile_provided){
    printf("You need to provide input!!!");
    help();
    return 0;
  }
  if (itersgiven==0){
    printf("You need to give num of iterations");
    help();
    return 0;
  }
  if (thread_provided==0){
  	printf("no threads given assuming single thread");
  	numthreads=1;
  }
  return 1;
}
unsigned long max3(unsigned long a,unsigned long b, unsigned long c){
	if (a>= b && a>=c) return a;
	if (b>=a && b>=c) return b;
	return c;
}
void readfile(char *fname, edge **edges,unsigned long *n,unsigned long *m){
  FILE *in_file=fopen(fname, "r");
  unsigned long tn=0,tm=0, u,v,ei;
  unsigned long e1=NLINKS;
  	*edges=(edge*)malloc(e1*sizeof(edge));

  while (fscanf(in_file,"%lu %lu", &u,&v)==2) {
		tn=max3(tn,u,v);
		(*edges)[tm].u=u;
    	(*edges)[tm].v=v;
		tm++;
		if (tm==e1) {
			e1+=NLINKS;
			*edges=(edge*)realloc(*edges,e1*sizeof(edge));
		}
	}
	fclose(in_file);
	*edges=(edge*)realloc(*edges,tm*sizeof(edge));
	*m=tm;
	*n=tn+1;

	printf("%lu, %lu \n",*n,*m);
}
void initdegrees(unsigned long  *d, edge *edges,unsigned long  m){
  unsigned long  ei;
  for ( ei=0;ei<m;++ei){
      d[edges[ei].u]++; 
      d[edges[ei].v]++;
  }
}
void create_csa(unsigned long  *d,unsigned long  n, unsigned long  m,unsigned long  *xids, unsigned long  *eids, unsigned long  *ids, edge *edges){
    xids[0]=0;
    unsigned long  i,ei;
    for (i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
    }
    for ( ei=0;ei<m;++ei){
      unsigned long  eu=edges[ei].u;
      unsigned long  ev=edges[ei].v;
      ids[xids[eu]++]=ev;
      ids[xids[ev]++]=eu;
    }
    xids[0]=0;
    for (i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
        eids[i-1]=xids[i]-1;
    }
}



int  binSearchClique(unsigned long  target,unsigned long  *array, unsigned long  l,unsigned long  r){
    int pos=-1;
    while (l<r){
      unsigned long  m = (l+r)/2;
      if (array[m]==target) {
        pos=1;
        break;
      }
      else if (array[m] > target){
        r=m;
      } else{
        l=m+1;
      }
    }
    return pos;
}
void cliqueRemoval(unsigned long  *cliqueSet,unsigned long  endcl,unsigned long  *xids,unsigned long  *eids,unsigned long  *ids,unsigned long  *d){ //removes clique from the graph
  qsort(cliqueSet,endcl,sizeof(unsigned long ),cmpfunc); //sort for binary search queries
  unsigned long i,j;
  for ( i=0;i<endcl;++i){
		unsigned long  x=cliqueSet[i];
		for ( j=xids[x];j<=eids[x];++j){
			unsigned long  z=ids[j];
			int pos=binSearchClique(z,cliqueSet,0,endcl); //see if endpoint is in the clique or not
      //printf("edge (%d,%d) on clique: %d \n",x,z,pos);
			if (pos != -1 ){ //if  in the clique, remove by swapping with last (and update ends array)
				d[x]--;
				ids[j]=ids[eids[x]];
				j--;
				eids[x]--;
				if (d[x]==0)
					break;
			}
		}
	}
}
void printgraph(unsigned long *xids,unsigned long *eids,unsigned long  *ids,unsigned long  *d,unsigned long  n){
  for (unsigned long  i=0;i<n;++i){
    if (d[i]>0){
    	    printf("%lu (%lu) [%lu,%lu]: ",i,d[i],xids[i],eids[i]);

    	for (unsigned long  j=xids[i];j<=eids[i];++j){
      		printf("%lu ",ids[j]);
    	}
    	printf("\n");
	}
  }
}
void cliqueFinderMethod1(vid *order, unsigned long *xids,unsigned long *eids,unsigned long *ids, unsigned long *d,unsigned long n,unsigned long *cliqueSet,unsigned long *endcl){
  /*
  *Adapted by: 
  *   Fast Algorithms for the Maximum Clique Problem on Massive Sparse Graphs
  *   Bharath Pattabiraman, Md. Mostofa Ali Patwary,Assefaw H. Gebremedhin,Wei-keng Liao, and Alok Choudhary
  *   -----
  *   https://arxiv.org/pdf/1209.5818.pdf
  */
	//printgraph(xids,eids,ids,d,n);
	omp_set_num_threads(numthreads);
  unsigned long **tempClique=(unsigned long**)malloc(numthreads*sizeof(unsigned long*));
  atomic_flag sizecheck;
  sizecheck.clear();
  unsigned long long **round=(unsigned long long**)malloc(numthreads*sizeof(unsigned long long*));
  unsigned long  long **bround=(unsigned long long**)malloc(numthreads*sizeof(unsigned long long*));	
  unsigned long  *br=(unsigned long*)malloc(numthreads*sizeof(unsigned long));
  unsigned long *r=(unsigned long*)malloc(numthreads*sizeof(unsigned long));
    unsigned long *bestsize=(unsigned long*)malloc(numthreads*sizeof(unsigned long));

  for (int i=0;i<numthreads;++i){
  	br[i]=0;
  	r[i]=0;
  	tempClique[i]=(unsigned long*)malloc(n*sizeof(unsigned long));
  	round[i]=(unsigned long long*)malloc(n*sizeof(unsigned long long));
  	bround[i]=(unsigned long long*)malloc(n*sizeof(unsigned long long));
  	bestsize[i]=0;	
  }
  unsigned long currmax=0;
  unsigned long  maxd=0;
  int maxc=-1;
  unsigned long ii;
  for (ii=0;ii<n;++ii){
  	for (int pid=0;pid<numthreads;++pid){ 
    	round[pid][ii]=0;
    	bround[pid][ii]=0;
	}
  }
	int o[numthreads];
	for (int i=0;i<numthreads;++i)
		o[i]=0;
  //	#pragma omp parallel for schedule(dynamic,4)
	#pragma omp parallel for schedule(static) 
	for ( ii=0;ii<n;++ii){ 
		int pid =omp_get_thread_num();
		//printf("%d - %d \n",pid,ii);
		unsigned long i=order[n-1 -ii].v;		
		if (d[i] > currmax && d[i]>0){ //prune 1
			 o[pid]++;
			  unsigned long  iii,j;
			  //printf("go into %d ",pid);
			 unsigned long  endtemp=0;
    	     unsigned long  cands=0;
        	if (r[pid]==maxlimit){
        		if (br[pid]==maxlimit)
        				printf("Limit Surpassed!!!");
        			br[pid]++;
        			r[pid]=0;
        	}else{
        		r[pid]++;	
        	}
			 for (j=xids[i];j<=eids[i];++j){ //iterate over i's vertices
         		unsigned long  z=ids[j];
          		if (d[z] > currmax){ //prune 3
            		round[pid][z]=r[pid];  //signal that z is in i's neighborhood  (i.e., e U)
              		bround[pid][z]=br[pid];
              		cands++;
              		if (maxc==-1){ //also find vertex of max. degree in this iteration
                		maxc=1;
                		maxd=z;
            		}
            	  	else{
                		if (d[maxd] < d[z])
                  		maxd=z;
              		}
          		}
           	}
       		tempClique[pid][endtemp++]=i; //add i to clique
       		while (cands!=0){ //while there exist candidate vertices!
          		if (endtemp+cands <= currmax){
          			//printf("pid: %d | I break because %d + %d < %lu \n",pid,endtemp,cands,currmax);
          			break;
          		}
          		unsigned long  x=maxd;
          		tempClique[pid][endtemp++]=x;   //add to the clique the max. degree one 
    		      //printf("pid: %d {%d : adding %d}  [%d %d ,%d %d]\n",pid,i,x,br[pid]-bround[pid][x],bround[pid][x],r[pid]-round[pid][x],round[pid][x]);
          		cands=0; //set cands to 0  (to recalculate!)
          		maxc=-1; 
          //r++; //update r to update U  implicitly
          		 if (r[pid]==maxlimit){
        			if (br[pid]==maxlimit)
        				printf("Limit Surpassed!!!");
        			br[pid]++;
        			r[pid]=0;
        		}else{
        			r[pid]++;	
        		}
          	for ( j=xids[x];j<=eids[x];++j){ //iterate over x's neighbors 
              	unsigned long  w=ids[j];
             // printf("pid: %d  |[%d<=%d<=%d] %d-%d are neighbors \n",pid,xids[x],j,eids[x],x,w);
              if (w!=i && d[w]>currmax &&  (bround[pid][w]==br[pid] && round[pid][w]==r[pid]-1) || (br[pid]>0 && bround[pid][w]==br[pid]-1 && r[pid]==0 && round[pid][w]==maxlimit)){ //prune 5 and check if part of U
                  round[pid][w]=r[pid]; //update condition for U
                  bround[pid][w]=br[pid];
                  cands++; //
                  if (maxc==-1){ //also update max. degree
                    	maxc=1;
                    	maxd=w;
                	}
                  else{
                    if (d[maxd]  < d[w])
                      maxd=w;
                  } 

              }       
          }
       } 
       		if (currmax  < endtemp) { //if the clique is  supposedlylarger than the largest found
          		 while (atomic_flag_test_and_set(&sizecheck)){
          		 	//printf("stuck here!");
          		 }
          		 if (currmax< endtemp){ //if it indeed is
          			//printf("%d (%d): ",pid,endtemp);
          			for (  iii=0;iii<endtemp;++iii){ 
            			cliqueSet[iii]=tempClique[pid][iii]; //replace!
            		//	printf("%d,%d ",cliqueSet[iii],tempClique[pid][iii]);
          			}
          		//	printf("\n");
          			currmax=endtemp;
          			//printf("pid: %d | currmax becomes %lu \n",pid,currmax);

          		}
          		sizecheck.clear();
      } 
     } else{
     	//printf("%d is ded \n",pid);
     }
	}
	#pragma omp parallel
	for ( ii=0;ii<n;++ii){ 
		int pid =omp_get_thread_num();
		//printf("%d - %d \n",pid,ii);
		unsigned long i=order[n-1 -ii].v;		
		if (d[i] == currmax && d[i]>0){ //prune 1
			 o[pid]++;
			  unsigned long  iii,j;
			  //printf("go into %d ",pid);
			 unsigned long  endtemp=0;
    	     unsigned long  cands=0;
        	if (r[pid]==maxlimit){
        		if (br[pid]==maxlimit)
        				printf("Limit Surpassed!!!");
        			br[pid]++;
        			r[pid]=0;
        	}else{
        		r[pid]++;	
        	}
			 for (j=xids[i];j<=eids[i];++j){ //iterate over i's vertices
         		unsigned long  z=ids[j];
          		if (d[z] > currmax){ //prune 3
            		round[pid][z]=r[pid];  //signal that z is in i's neighborhood  (i.e., e U)
              		bround[pid][z]=br[pid];
              		cands++;
              		if (maxc==-1){ //also find vertex of max. degree in this iteration
                		maxc=1;
                		maxd=z;
            		}
            	  	else{
                		if (d[maxd] < d[z])
                  		maxd=z;
              		}
          		}
           	}
       		tempClique[pid][endtemp++]=i; //add i to clique
       		while (cands!=0){ //while there exist candidate vertices!
          		if (endtemp+cands <= currmax){
          			//printf("pid: %d | I break because %d + %d < %lu \n",pid,endtemp,cands,currmax);
          			break;
          		}
          		unsigned long  x=maxd;
          		tempClique[pid][endtemp++]=x;   //add to the clique the max. degree one 
    		      //printf("pid: %d {%d : adding %d}  [%d %d ,%d %d]\n",pid,i,x,br[pid]-bround[pid][x],bround[pid][x],r[pid]-round[pid][x],round[pid][x]);
          		cands=0; //set cands to 0  (to recalculate!)
          		maxc=-1; 
          //r++; //update r to update U  implicitly
          		 if (r[pid]==maxlimit){
        			if (br[pid]==maxlimit)
        				printf("Limit Surpassed!!!");
        			br[pid]++;
        			r[pid]=0;
        		}else{
        			r[pid]++;	
        		}
          	for ( j=xids[x];j<=eids[x];++j){ //iterate over x's neighbors 
              	unsigned long  w=ids[j];
             // printf("pid: %d  |[%d<=%d<=%d] %d-%d are neighbors \n",pid,xids[x],j,eids[x],x,w);
              if (w!=i && d[w]>currmax &&  (bround[pid][w]==br[pid] && round[pid][w]==r[pid]-1) || (br[pid]>0 && bround[pid][w]==br[pid]-1 && r[pid]==0 && round[pid][w]==maxlimit)){ //prune 5 and check if part of U
                  round[pid][w]=r[pid]; //update condition for U
                  bround[pid][w]=br[pid];
                  cands++; //
                  if (maxc==-1){ //also update max. degree
                    	maxc=1;
                    	maxd=w;
                	}
                  else{
                    if (d[maxd]  < d[w])
                      maxd=w;
                  } 

              }       
          }
       } 
       		if (currmax  < endtemp) { //if the clique is  supposedlylarger than the largest found
          		 while (atomic_flag_test_and_set(&sizecheck)){
          		 	//printf("stuck here!");
          		 }
          		 if (currmax< endtemp){ //if it indeed is
          			//printf("%d (%d): ",pid,endtemp);
          			for (  iii=0;iii<endtemp;++iii){ 
            			cliqueSet[iii]=tempClique[pid][iii]; //replace!
            		//	printf("%d,%d ",cliqueSet[iii],tempClique[pid][iii]);
          			}
          		//	printf("\n");
          			currmax=endtemp;
          			//printf("pid: %d | currmax becomes %lu \n",pid,currmax);

          		}
          		sizecheck.clear();
      } 
     } else{
     	//printf("%d is ded \n",pid);
     }
	}
  *endcl=currmax;
  for (int i=0;i<numthreads;++i)
  	printf("%d ", o[i]);
  printf("\n");
  for (int i=0;i<numthreads;++i){
  	free(tempClique[i]);
  	free(round[i]);
  	free(bround[i]);
  }
  free(tempClique);
  free(round);
  free(bround);
  free(r);
  free(br);
  free(bestsize);
} 
void cliqueFinderMethod2(int *xids,int *eids,int *ids, int *d,int n){
  //maybe nother day
}

void iterativeCliqueFind(unsigned long iters,vid *order, unsigned long  *xids,unsigned long  *eids,unsigned long  *ids,unsigned long  *d,unsigned long  n){
  unsigned long  *cliqueSet=(unsigned long *)malloc(n*sizeof(unsigned long ));
  unsigned long  endcl=0;
  if (iters==-1){
   	iters=xids[n]/2;
  }unsigned long i;
  for ( i=0;i<iters;++i){
    double t0=u_seconds();
      cliqueFinderMethod1(order,xids,eids,ids,d,n,cliqueSet,&endcl);
    double t1=u_seconds();
    printf("Iteration %d : %lu  took %.2f seconds ",i,endcl,t1-t0);
   // fprintf(fp,"%d %lu %.2f \n",i,endcl,t1-t0);
    if (endcl<=0){
      printf("EMPTY GRAPH \n");
      break;
    }
    /*for (int z=0;z<endcl;++z){
      printf("%d ",cliqueSet[z]);
    }*/
    printf("\n");
    t0=u_seconds();
      cliqueRemoval(cliqueSet,endcl,xids,eids,ids,d);
    t1=u_seconds();
    printf("Clique removal took %.2f seconds \n",t1-t0);
  }
  free(cliqueSet);
}
int main(int argc, char *argv[]) {
    if(!parse_args(argc,argv)){
    return -1;
  } 
  printf("Running on %s \n",mfile);
    unsigned long  n,m;
    unsigned long  *d;
    edge *edges;
    double t0= u_seconds();
        readfile(mfile,&edges,&n,&m);
    double t1= u_seconds();
    printf("Reading input  took %.2f seconds \n",t1-t0);
    unsigned long  sum=0;
    d=(unsigned long *)calloc(n,sizeof(unsigned long ));
    initdegrees(d,edges,m);
    vid *order=(vid*)malloc(n*sizeof(vid));
    unsigned long i;
    for (i=0;i<n;++i){
    	order[i].v=i;
    	order[i].d=rand();
    }
    qsort(order,n,sizeof(vid),cmpfunc1);
    for (i=0;i<n;++i){
    	order[i].d=d[order[i].v];
    }
    unsigned long piecesize=n/numthreads;
    unsigned long  rem=n;
    for (int i=0;i<numthreads;++i){
    	unsigned long  si=piecesize*i;
    	unsigned long  pi=piecesize;
    	if (i==numthreads-1){
    		pi=rem;
    	}
    	rem-=pi;
    	printf("%lu until %lu \n",si,si+pi-1);
    	qsort(&order[si],pi,sizeof(vid),cmpfunc1); //sort for binary search queries
    }
    printf("\n");
    unsigned long  *xids=(unsigned long *)malloc((n+1)*sizeof(unsigned long ));
    unsigned long  *eids=(unsigned long *)malloc((n)*sizeof(unsigned long ));
    unsigned long  *ids=(unsigned long *)malloc(2*m*sizeof(unsigned long ));
    t0= u_seconds();
        create_csa(d,n,m,xids,eids,ids,edges);
    t1=u_seconds();  
    printf("Creating csv took %.2f seconds \n",t1-t0);
    free(edges);
    t0= u_seconds();
    unsigned long uliters=iters;
    iterativeCliqueFind(uliters,order,xids,eids,ids,d,n);
   t1=u_seconds();  
       printf("Iterative Clique took %.2f seconds\n",t1-t0); 
    printf("------- \n");
    
    free(order);
    free(d);
    free(xids);
    free(ids);
    free(eids);
  return 0;
}