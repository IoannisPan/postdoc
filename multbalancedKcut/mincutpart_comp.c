//#include "partition.h"
#include "utils.h"
#include "struct.h"
#include <stdlib.h>
#include <stdio.h>
void KLrefinementcomp(adjlist *g, unsigned long *lab1,unsigned long *vids, unsigned long nvids,unsigned long id0,unsigned long id1,unsigned long nmax,int *still){
	 	long long *G=(long long *)malloc(nmax*sizeof(long long));
 		xe *xeL1=(xe*)malloc(nvids*2 *sizeof(xe));
 		xe *xeL2=(xe*)malloc(nvids*2 *sizeof(xe));
		unsigned long *order=(unsigned long *)malloc(nmax*sizeof(unsigned long));
 		unsigned long *L1=(unsigned long *)malloc(nvids*sizeof(unsigned long));
 		unsigned long *dL1=(unsigned long *)malloc(2*nvids*sizeof(unsigned long));
 		unsigned long *L2=(unsigned long *)malloc(nvids*sizeof(unsigned long));
 		unsigned long *dL2=(unsigned long *)malloc(2*nvids*sizeof(unsigned long));
 		unsigned long offset=nvids;  
 		gpair *gpairs=(gpair*)malloc(nvids/2 * sizeof(gpair));
 		int changes=1;
 		unsigned long n0=0;
 		unsigned long n1=0;
 		for (unsigned long i=0;i<nvids;++i){
 			unsigned long u=vids[i];
 			if (lab1[u]==id0) n0++;
 			else if (lab1[u]==id1) n1++;
 		}
 		while (changes==1){
 			changes=0;
 			for (unsigned long ii=0;ii<2*nvids;++ii) {
 				dL1[ii]=0;
 			 	dL2[ii]=0;
		
 			}
	 		for (unsigned long i=0;i<nvids;++i){
 				unsigned long u=vids[i];
 				G[u]=0; 				
 				if (lab1[u]==id0){
 					for (unsigned long j=g->cd[u];j<g->cd[u+1];++j){
 						unsigned long v=g->adj[j];
 						if (still[v]==1){
 							if (lab1[v]==id1)
 								G[u]--;
 							else if (lab1[v]==id0);
 								G[u]++;
 						}
 					}
 					dL1[ll2ul(offset,G[u])]++;

 				}else if (lab1[u]==id1){
 					for (unsigned long j=g->cd[u];j<g->cd[u+1];++j){
 						unsigned long v=g->adj[j];
 						if (still[v]==1){
 							if (lab1[v]==id0)
 								G[u]--;
 							else if (lab1[v]==id1);
 								G[u]++;
 						}
 					}
 					dL2[ll2ul(offset,G[u])]++;

 				}

 			}
 			unsigned long head1L,head1R,head2L,head2R;
 			create_init_ds(&head1L,&head1R,offset, dL1,G ,xeL1,vids, nvids,0,id0,order, L1,lab1);
 			create_init_ds(&head2L,&head2R,offset, dL2,G ,xeL2,vids, nvids,0,id1,order, L2,lab1);
			for (unsigned long ii=0;ii<n1;++ii){
 			 	if (n1>n0 && ii==n0) 
 			 		break; 			 
 			 	unsigned long v1=head1R;
 			 	unsigned long v2=head2R;
				long long int  upper_bound=G[L1[xeL1[v1].xL]]+G[L2[xeL2[v2].xL]];
				long long int lower_bound=upper_bound-2;
				unsigned long  lb1=v1-1;
				if (v1==0) 
					lb1=0;
				unsigned long  lb2=v2-1;
				if (v2==0)
					lb2=0;

				long long int curr=0;
				int first=1;
				unsigned long curr_u=0;
				unsigned long  curr_v=0;
				unsigned long optfound=0;
			while (1){
				if (v1<lb1 || optfound==1)  //if v1 drops below on its lower bound stop or upper bound has been met
					break;
				for (unsigned long  j1=xeL1[v1].xL;j1<=xeL1[v1].eL;++j1){
					if (optfound)
						break;
					unsigned long  u1=L1[j1];
					unsigned long  tmpv2=v2;
					unsigned long  gu1u2lb=0;
					while (1){
						if (tmpv2<lb2) {//if v2 drops below its lower bound stop
							break;
						}
						for (unsigned long j2=xeL2[tmpv2].xL;j2<=xeL2[tmpv2].eL;++j2){
							unsigned long u2=L2[j2];
							if (G[u1] + G[u2]<= lower_bound){
								gu1u2lb=1;
								break;
							}
							long long int p=binSearch(u1,u2,g);
							long long int  tempg=G[u1]+G[u2] -2*p; //gain from the swap
							if (first==1){   //update max. gain, if it becomes equal to the upper bound stop
								first=0;
								curr_u=u1;curr_v=u2; curr=tempg;
								if (curr>lower_bound) lower_bound=curr;
								if (curr==upper_bound){
									optfound=1;
									break;
								}
							}else if (tempg > curr ){
								curr_u=u1;curr_v=u2; curr=tempg;
								if (curr>lower_bound) lower_bound=curr;
								if (curr==upper_bound){
									optfound=1;
									break;
								}
							}
							if (p==0) //if meet  non-neighbor, stop because we have found the best for u1
								break;
						} if (gu1u2lb)
							break;
						if (optfound)
							break;
						if (xeL2[tmpv2].has_prev)
							tmpv2=xeL2[tmpv2].previous; //all items in L2 with G = v2 have been explored, go to v2-1 (if exists)
						else
							break;
					}
				
				}
				if (xeL1[v1].has_prev==1)	
					v1=xeL1[v1].previous;//all items in L1 with G = v1 have been explored, go to v1 -1 (if exists)
				else
					break;			
				upper_bound--;
			}
			gpairs[ii].a=curr_u; //save the best found
			gpairs[ii].b=curr_v;
			gpairs[ii].gain=curr;
			delete(curr_u,ll2ul(offset,G[curr_u]), order,xeL1,L1,&head1L,&head1R);
			delete(curr_v,ll2ul(offset,G[curr_v]), order,xeL2,L2,&head2L,&head2R);
			order[curr_u]=3*offset;
			order[curr_v]=3*offset;
		 	update_on_pair_comp(offset, G,g,curr_u,curr_v,&head1L, &head1R,xeL1, L1,&head2L,&head2R,xeL2, L2,lab1,still,order);
 			}
 			unsigned long  maxp=0;
		long long int  maxgain=gpairs[0].gain ;
		long long int currgain=maxgain;
		for (unsigned long  i=1;i<n1;++i){
			if (n0 < n1 && i==n0)
				break;			
			currgain+=gpairs[i].gain;
			if (currgain> maxgain){
				maxp=i;
				maxgain=currgain;
			}
		}
		if (maxgain>0){
			for (unsigned long  i=0;i<=maxp;++i){
				unsigned long  a=gpairs[i].a;
				unsigned long  b=gpairs[i].b;

				lab1[a]=id1;
				lab1[b]=id0;
				//printf("swap %lu <---> %lu \n",a,b);
			}
			changes=1;
		}
 		}
 		free(order);
 		free(L1);
 		free(dL1);
 		free(L2);
 		free(dL2);
 		free(gpairs);
 		free(xeL1);
 		free(xeL2);
 		free(G);

}
void greedyMincutComp(adjlist *g, unsigned long *lab1,unsigned long l0,unsigned long l1,unsigned long *vids, unsigned long nvids,int *still,unsigned long nmax) {
	unsigned long u,v,i,j,s=0;
	unsigned iter=0;
	unsigned long cut=0;
	for (i=0;i<nvids;i++){
		v=vids[i];
		lab1[v]=rand() > (RAND_MAX / 2);
		if (lab1[v]==0){
			s++;
			lab1[v]=l0;
		}else{
			lab1[v]=l1;
		}
	}

	//avoids all nodes on one side:
	if (s==nvids){
		lab1[vids[0]]=l1;
		s--;
	}
	else if (s==0){
		lab1[vids[0]]=l0;
		s++;
	}
	int b;
	unsigned long *d0=calloc(nmax,sizeof(unsigned long)),*d1=calloc(nmax,sizeof(unsigned long));
	for (i=0;i<nvids;i++){
		u=vids[i];
		for (j=g->cd[u];j<g->cd[u+1];++j){
			v=g->adj[j];
			if (still[v]==1){
				if (lab1[v]==l0)
					d0[u]++;
				else
					d1[u]++;
			}
		}
	}


	do {//greedy heuristics
		iter++;
		b=0;
			for (i=0;i<nvids;i++){
			u=vids[i];
			if (lab1[u]==l0 && s>1 &&  ( ((s-1) - d0[u]) <  ((nvids - s) - d1[u]))){				
				b=1;
				s--;
				lab1[u]=l1;
				for (j=g->cd[u];j<g->cd[u+1];j++){
					v=g->adj[j];
					if (still[v]==1){
						d0[v]--;
						d1[v]++;
					}
				}
			}
			else if (lab1[u]==l1 && nvids-s>1 &&  ( ((nvids-1) - d1[u]) <  (s - d0[u]))){			
				b=1;
				s++;
				lab1[u]=l0;
				for (j=g->cd[u];j<g->cd[u+1];j++){
					v=g->adj[j];
					if (still[v]==1){
						d0[v]++;
						d1[v]--;
					}
				}
			}
		}
	} while (b==1);
	
	//printf("Number of iterations = %u\n",iter);
	//printf("cut, s, n-s = %lf %lu %lu\n",cut,s,n-s);
	free(d0);
	free(d1);
}
double mincut2(unsigned long nt1,unsigned long nt2,unsigned long *ntf, adjlist *g, unsigned long *lab1,unsigned long *vids, unsigned long nvids,unsigned long id0,unsigned long id1,unsigned long nmax,int *still){
 	unsigned long n0=0;
 	unsigned long n1=0;
 	unsigned long *order=(unsigned long *)malloc(nmax*sizeof(unsigned long));
 	unsigned long *L=(unsigned long *)malloc(nvids*sizeof(unsigned long));
 	unsigned long *dL=(unsigned long *)malloc(2*nvids*sizeof(unsigned long));
 	long long *G=(long long *)malloc(nmax*sizeof(long long));
 	xe *xeL=(xe*)malloc(nvids*2 *sizeof(xe));
 	unsigned long offset=nvids;  
 	 	greedyMincutComp(g,lab1, id0,id1,vids, nvids,still, nmax);  	//call min-cut on g for lab1
//call min-cut on g for lab1
 	for (unsigned long i=0;i<nvids;++i){
 		if (lab1[vids[i]]==id0)
 			n0++;
 		else
 			n1++;
 	}
 	if (n1 > n0){
 		for (unsigned long i=0;i<nvids;++i){
 			if (lab1[vids[i]]==id0)
 				lab1[vids[i]]=id1;
 			else
 				lab1[vids[i]]=id0;
 		}
 		int t=n0;
 		n0=n1;
 		n1=t;
 	}/*
 		for (unsigned long i=0;i<nvids;++i){
 		lab1[vids[i]]=id0;
 	}
 /* lab1[2]=id1;   lab1[5]=id1; lab1[7]=id1; lab1[9]=id1; lab1[10]=id1;
  n0=8; n1=5; */
 	//	printf("sizes: %lu instead of  %lu \n", n1,nt);
 		if (n1==0)
 			exit(0);
 		unsigned long ntmin=nt1;
 		unsigned long ntmax=nt2;
 		if (nt1 > nt2){
 			ntmin=nt2;
 			ntmax=nt1;
 		}
 		if (nt1!=n1 || nt2!=n1 ){
 			unsigned long nt=ntmax;
 			if (ntmin > n1){
 				nt=ntmin;
 			}
 			for (unsigned long ii=0;ii<2*nvids;++ii) 
 				dL[ii]=0;
 			unsigned long lw;
 			long long gmax=0;
 			if (nt>n1)
 				lw=id0;  //create ll on n0
 			else
 				lw=id1; 	//create ll on n1
 			for (unsigned long ii=0;ii<nvids;++ii){
 				unsigned long v=vids[ii];
 				//printf("%lu is label = %lu \n",v,lab1[v]);
 				if (lab1[v]==lw){
 					G[v]=0;
 				 		for (unsigned long i=g->cd[v];i<g->cd[v+1];++i){
							unsigned long u=g->adj[i];
							if (still[u]==1){
							 	if (lab1[u]!=lab1[v])
									G[v]++;
								else
									G[v]--;
							}						
 					}
 					//printf("adding %lu with  %lld \n ",v,G[v]);
 					dL[ll2ul(offset,G[v])]++;
 					if (G[v] > gmax)
 					 	gmax=G[v];
 				}
 			}
 			unsigned long headL,headR;
 			create_init_ds(&headL,&headR,offset, dL,G ,xeL,vids, nvids,gmax,lw,order, L,lab1);
 		//	exit(0);
 		while (n1!=nt){
 			unsigned long v=L[xeL[headL].xL];
 		//	printf("after delete %lu \n",v);
 			delete(v,ll2ul(offset,G[v]), order,xeL,L,&headL,&headR);
 			 		//	printL(head,xeL,L);

 			if (lab1[v]==id1){
 				n1--;
 				lab1[v]=id0;
 			}else{
 				n1++;
 				lab1[v]=id1;
 			}
 			if (n1==nt)
 				break;
 			update_on_v_comp(offset, G,g,v, &headL,&headR, xeL, order,L,lw,lab1,still);
 			//printf("after update \n");
 			//printL(head,xeL,L);
 			//exit(0);
 		}

 	}
 	//KLrefinementcomp(g, lab1,vids, nvids,id0,id1,nmax,still);
 	double cut=n0*n1;
 	for (unsigned long ii=0;ii<nvids;++ii){
 		unsigned long v=vids[ii];
 		if (lab1[v]==id0){
 			for (unsigned long j=g->cd[v];j<g->cd[v+1];++j){
 				unsigned long u=g->adj[j];
 				if (still[u]==1 && lab1[u]!=lab1[v])
 					cut--;
 			}
 		}
 	}
 	*ntf=n1;
 	free(xeL);
 	free(dL);
 	free(order);
 	free(L);
 	return cut;
}

