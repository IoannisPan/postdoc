#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <strings.h>//to use "bzero"
#include <time.h>//to estimate the runing time
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>

#include "struct.h"
#include "multicut.h"
#define NLINKS 100000000 //maximum number of edges of the input graph: used for memory allocation, will increase if needed. //NOT USED IN THE CURRENT VERSION
#define NNODES 10000000 //maximum number of nodes in the input graph: used for memory allocation, will increase if needed
#define HMAX 100 //maximum depth of the tree: used for memory allocation, will increase if needed

int cmpfunc (const void * a, const void * b){
  if (*(unsigned long *)a>*(unsigned long *)b){
    return 1;
  }
  return -1;
}
unsigned long *arr1;
unsigned long *arr2;
unsigned long nne;
int iters;
//compute the maximum of three unsigned long
 unsigned long max3(unsigned long a,unsigned long b,unsigned long c){
  a = (a > b) ? a : b;
  return (a > c) ? a : c;
}

//reading the edgelist from file
double  spacecut=0;
double spacebit=0;
//reading the list of edges and building the adjacency array
adjlist* readadjlist(char* input){
  unsigned long n1=NNODES,n2,u,v,i;
  unsigned long *d=calloc(n1,sizeof(unsigned long));
  adjlist *g=malloc(sizeof(adjlist));
  FILE *file;

  g->n=0;
  g->e=0;
  file=fopen(input,"r");//first reading to compute the degrees
  while (fscanf(file,"%lu %lu", &u, &v)==2) {
    g->e++;
    g->n=max3(g->n,u,v);
    if (g->n+1>=n1) {
      n2=g->n+NNODES;
      d=realloc(d,n2*sizeof(unsigned long));
      bzero(d+n1,(n2-n1)*sizeof(unsigned long));
      n1=n2;
    }
    d[u]++;
    d[v]++;
  }
  fclose(file);

  g->n++;
  d=realloc(d,g->n*sizeof(unsigned long));

  g->cd=malloc((g->n+1)*sizeof(unsigned long long));
  g->cd[0]=0;
  for (i=1;i<g->n+1;i++) {
    g->cd[i]=g->cd[i-1]+d[i-1];
    d[i-1]=0;
  }

  g->adj=malloc(2*g->e*sizeof(unsigned long));

  file=fopen(input,"r");//secong reading to fill the adjlist
  while (fscanf(file,"%lu %lu", &u, &v)==2) {
    g->adj[ g->cd[u] + d[u]++ ]=v;
    g->adj[ g->cd[v] + d[v]++ ]=u;
  }
  fclose(file);


  g->map=NULL;

  free(d);

  return g;
}
adjlist* ignorezeros(adjlist *g){
  unsigned long *inside=(unsigned long*)malloc(g->n*sizeof(unsigned long));
  unsigned long *pos=(unsigned long*)malloc(g->n*sizeof(unsigned long));
  adjlist *ng=(adjlist*)malloc(sizeof(adjlist));
  unsigned long nn=0;
  unsigned long gn=g->n;
  for (unsigned long i=0;i<gn;++i){
    unsigned long di=g->cd[i+1] - g->cd[i];
    if (di>0){
      inside[i]=1;
      pos[i]=nn;
      nn++;
    }else
      inside[i]=0;
  }
  ng->n=nn;
  ng->e=g->e;
  ng->map=NULL;
  ng->adj=(unsigned long*)malloc(2*g->e*sizeof(unsigned long));
  ng->cd=(unsigned long*)malloc((1+nn)*sizeof(unsigned long));
  for (unsigned long i=0;i<gn;++i){
    if (inside[i]==1){
       ng->cd[pos[i]]=g->cd[i];
    }
  }
  ng->cd[nn]=2*g->e;
  for (unsigned long i=0;i<2*g->e;++i)
    ng->adj[i]=pos[g->adj[i]];
  for (unsigned long i=0;i<ng->n;++i){
    unsigned long di=ng->cd[i+1] - ng->cd[i];
    qsort(&ng->adj[ng->cd[i]],di,sizeof(unsigned long),cmpfunc);
  
  }
  free(pos);
  free(inside);
  return ng;
}
//reading the list of edges and building the adjacency array
//NOT USED IN THE CURRENT VERSION
#define BUFFER_SIZE (16 * 1024)

int
read_two_integers(int fd, unsigned long *u, unsigned long *v) {
  static char buf[BUFFER_SIZE];
  static ssize_t how_many = 0;
  static int pos = 0;
  unsigned long node_number=0;
  int readu = 0;

  while (1) {
    while(pos < how_many) {
      if (buf[pos] == ' ') {
	*u = node_number;
	readu=1;
	node_number = 0;
	pos++;
      } else if (buf[pos] == '\n') {
	*v = node_number;
	node_number = 0;
	readu=0;
	pos++;
	return 2;
      } else {
	node_number = node_number * 10 + buf[pos] - '0';
	pos++;
      }
    }

    how_many = read (fd, buf, BUFFER_SIZE);
    pos = 0;
    if (how_many == 0) {
      if(readu==1) {
	*v = node_number;
	return 2;
      }	    
      return 0;;
    }
  } 
}

adjlist *
readadjlist_v2(char* input_filename){
  unsigned long n1 = NNODES, n2, u, v, i;
  unsigned long *d = calloc(n1,sizeof(unsigned long));
  adjlist *g = malloc(sizeof(adjlist));

  g->n = 0;
  g->e = 0;

  // first read of the file to compute degree of each node
  int fd = open(input_filename, O_RDONLY);
  posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
  while (read_two_integers(fd, &u, &v) == 2) {
    g->e++;
    g->n = max3(g->n, u, v);
    if (g->n + 1 >= n1) {
      n2 = g->n+NNODES;
      d = realloc(d, n2 * sizeof(unsigned long));
      bzero(d + n1, (n2 - n1) * sizeof(unsigned long));
      n1 = n2;
    }
    d[u]++;
    d[v]++;
  }
  close(fd);

  g->n++;
  d = realloc(d, g->n * sizeof(unsigned long));

  // computation of cumulative degrees
  g->cd = malloc((g->n + 1) * sizeof(unsigned long long));
  g->cd[0] = 0;
  for (i = 1; i < g->n + 1; i++) {
    g->cd[i] = g->cd[i - 1] + d[i - 1];
    d[i - 1] = 0;
  }

  g->adj = malloc(2 * g->e * sizeof(unsigned long));

  // second read to create adjlist
  fd = open(input_filename, O_RDONLY);
  posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
  while (read_two_integers(fd, &u, &v) == 2) {
    g->adj[g->cd[u] + d[u]] = v;
    d[u]++;
    g->adj[g->cd[v] + d[v]] = u;
    d[v]++;
  }
  close(fd);


  g->map = NULL;

  free(d);

  return g;
}

//freeing memory
void free_adjlist(adjlist *g){
  free(g->cd);
  free(g->adj);
  free(g->map);
  free(g);
}



//Make the nlab subgraphs of graph g using the labels "lab". Make the subgraphs ne by one...
adjlist* mkchild(adjlist* g, unsigned long* lab, unsigned long nlab, unsigned h, unsigned long clab){
  unsigned long i,u,v,lu;
  unsigned long long j,k,tmp;

  static unsigned hmax=0;
  static unsigned long **nodes;
  static unsigned long **new;
  static unsigned long long **cd;
  static unsigned long long **e;
  static unsigned long *d;
  adjlist* sg;

  if (hmax==0){
    hmax=HMAX;
    nodes=malloc(HMAX*sizeof(unsigned long *));
    new=malloc(HMAX*sizeof(unsigned long *));
    cd=malloc(HMAX*sizeof(unsigned long long *));
    e=malloc(HMAX*sizeof(unsigned long long *));
  }
  if (h==hmax){
    hmax+=HMAX;
    nodes=realloc(nodes,hmax*sizeof(unsigned long *));
    new=realloc(new,hmax*sizeof(unsigned long *));
    cd=realloc(cd,hmax*sizeof(unsigned long long *));
    e=realloc(e,hmax*sizeof(unsigned long long *));
  }

  if (clab==0){
    d=calloc(nlab,sizeof(unsigned long));
    cd[h]=malloc((nlab+1)*sizeof(unsigned long long));
    e[h]=calloc(nlab,sizeof(unsigned long long));
    for (i=0;i<g->n;i++){
      d[lab[i]]++;
    }
    cd[h][0]=0;
    for (i=0;i<nlab;i++){
      cd[h][i+1]=cd[h][i]+d[i];
      d[i]=0;
    }
    nodes[h]=malloc(g->n*sizeof(unsigned long));
    new[h]=malloc(g->n*sizeof(unsigned long));
    for (u=0;u<g->n;u++){
      lu=lab[u];
      nodes[h][cd[h][lu]+d[lu]]=u;
      new[h][u]=d[lu]++;
      for (j=g->cd[u];j<g->cd[u+1];j++){
	v=g->adj[j];
	if (lu==lab[v]){
	  e[h][lu]++;
	}
      }
    }
    free(d);
  }

  sg=malloc(sizeof(adjlist));
  sg->n=cd[h][clab+1]-cd[h][clab];
  sg->e=e[h][clab]/2;
  sg->cd=malloc((sg->n+1)*sizeof(unsigned long long));
  sg->cd[0]=0;
  sg->adj=malloc(2*sg->e*sizeof(unsigned long));
  sg->map=malloc(sg->n*sizeof(unsigned long));

  tmp=0;
  for (k=cd[h][clab];k<cd[h][clab+1];k++){
    u=nodes[h][k];
    sg->map[new[h][u]]=(g->map==NULL)?u:g->map[u];//new[h][u] is equal to i-cd[h][clab]...
    for (j=g->cd[u];j<g->cd[u+1];j++){
      v=g->adj[j];
      if (clab==lab[v]){//clab is equal to lab[u]
	       sg->adj[tmp++]=new[h][v];
      }
    }
    sg->cd[new[h][u]+1]=tmp;
  }

  if (clab==nlab-1){
    free(nodes[h]);
    free(new[h]);
    free(cd[h]);
    free(e[h]);
  }

  return sg;
}
void nextIterToFile(){
    FILE *fp=fopen("b.txt","w");
    for (unsigned long i=0;i<nne;++i){
      fprintf(fp,"%lu %lu \n",arr1[i],arr2[i]);
    }
    fclose(fp);
}
void addEdgesToCutset(unsigned long *lab, unsigned long k,adjlist *g){
  unsigned long *ni=(unsigned long*)malloc(k*sizeof(unsigned long));
  unsigned long *cicj=(unsigned long*)malloc(k*k*sizeof(unsigned long));
  unsigned long *verts=(unsigned long*)malloc(g->n*sizeof(unsigned long));
  unsigned long *dni=(unsigned long*)malloc((1+k)*sizeof(unsigned long));
  unsigned long *appeared=(unsigned long*)malloc(g->n*sizeof(unsigned long));
  int *bb=(int*)malloc(k*k*sizeof(int));
  for (unsigned long i=0;i<k;++i) 
    ni[i]=0;
    for (unsigned long i=0;i<k;++i) {
        for (unsigned long j=i+1;j<k;++j){
          cicj[i*k+ j]=0;
        }
    } 
  for (unsigned long i=0;i<g->n;++i){
    ni[lab[i]]++;
    appeared[i]=0;
    for (unsigned long j=g->cd[i];j<g->cd[i+1];++j){
        unsigned long z=g->adj[j];
        if (lab[i] < lab[z]){
          cicj[lab[i]*k + lab[z]]++;
        }
    }
  }
  dni[0]=0;
  unsigned long appearround=1;
  for (unsigned long i=1;i<=k;++i){
    dni[i]= dni[i-1] + ni[i-1];
    ni[i-1]=dni[i-1];
  }
  for (unsigned long i=0;i<g->n;++i){
    verts[ni[lab[i]]++]=i;
  }
  for (unsigned long i=0;i<k;++i){
    ni[i]=dni[i+1]-dni[i];
  }
for (unsigned long ci=0;ci<k;++ci){
    for (unsigned long cj=ci+1;cj<k;++cj){
      unsigned long pos=ci*k + cj;
      unsigned long cutcicj=cicj[pos];
      bb[pos]=0;
      if (cutcicj > ni[ci]*ni[cj] - cutcicj)
        bb[pos]=1;
    }
  } 

  for (unsigned long i=0;i<g->n;++i){
      appearround++;
      unsigned long li=lab[i];
      for (unsigned long j=g->cd[i];j<g->cd[i+1];++j){
        unsigned long z=g->adj[j];
        unsigned long lz=lab[z];
        if (li<lz){
          unsigned long pos=li*k + lz;
          if (bb[pos]==0){
            if (g->map==NULL){
              arr1[nne]=i;
              arr2[nne++]=z;
            }else{
              arr1[nne]=g->map[i];
              arr2[nne++]=g->map[z];
            }
            
          }else{
            appeared[z]=appearround;
          }
        }
      }
      for (unsigned long cj=li+1;cj<k;++cj){
        unsigned long pos=li*k + cj;
        if (bb[pos]==1){
          for (unsigned long cjj=dni[cj];cjj<dni[cj+1];++cjj){
              unsigned long zz=verts[cjj];
              if (appeared[zz]!=appearround){
                 if (g->map==NULL){
                arr1[nne]=i;
                arr2[nne++]=zz;
              }else{
                arr1[nne]=g->map[i];
                arr2[nne++]=g->map[zz];
              }
              }
          }
        }
      }
  }
  free(bb);
  free(verts);
  free(dni);
  free(ni);
  free(cicj);
  free(appeared);
}
//recursive function
void recurs(adjlist* g, unsigned h, FILE* file,unsigned long k){
  time_t t0,t1,t2;
  unsigned long nlab;
  unsigned long i;
  adjlist* sg;
  unsigned long *lab;

  if (h==0){
    t0=time(NULL);
  }


   // printf("~~~~~now on %lu with %lu edges \n",g->n,g->e);
    lab=malloc(g->n*sizeof(unsigned long));
    nlab=k;
    if (k>g->n)
      nlab=g->n;
    unsigned long mcedges=multkcut(g, lab,nlab);
   /* for (unsigned long i=0;i<g->n;++i){
      printf("%lu : %lu \n",i,lab[i]);
    }*/
    if (iters>1)
      addEdgesToCutset(lab, nlab,g);
    spacecut+=mcedges;
    spacebit+=(nlab*(nlab-1))/2;
    if (h==0) {
      t1=time(NULL);
      printf("First level partition computed: %lu parts\n", nlab);
      printf("- Time to compute first level partition = %ldh%ldm%lds\n",(t1-t0)/3600,((t1-t0)%3600)/60,((t1-t0)%60));
    }
    if (nlab==1){
      fprintf(file,"%u 1 %lu",h,g->n);
      for (i=0;i<g->n;i++){
	fprintf(file," %lu",g->map[i]);
      }
      fprintf(file,"\n");
    }
    else{
      fprintf(file,"%u %lu\n",h,nlab);
      for (i=0;i<nlab;i++){
         sg=mkchild(g,lab,nlab,h,i);
	      // printf(" %lu /%lu with %lu now to %lu  with %lu edges\n",i+1,nlab,g->n,sg->n,sg->e);
         if (sg->e > 0 && sg->e < ((sg->n) *(sg->n -1))/2)
          recurs(sg,h+1,file,k);
        else
          spacebit++;
      }
    }
    free_adjlist(g);
    free(lab);
  
}



//main function
int main(int argc,char** argv){
  adjlist* g;
  iters=atoi(argv[4]);
  double ge,gn;
  double extracomp=0.0;
  for (int iter=0;iter<iters;++iter){
  time_t t0=time(NULL),t1,t2;
  srand(time(NULL));
  spacebit=0;
  spacecut=0;
  printf("Reading edgelist from file %s and building adjacency array\n",argv[1]);
  if (iter==0)
    g=readadjlist(argv[1]);
  else
    g=readadjlist("b.txt");
  adjlist *ng=ignorezeros(g);
    free_adjlist(g);
	
  printf("Number of nodes: %lu\n",ng->n);
  printf("Number of edges: %lu\n",ng->e);
  if (ng->e==0)
    break;
  if (iter==0){
      arr1=(unsigned long*)malloc(ng->e*sizeof(unsigned long));
      arr2=(unsigned long*)malloc(ng->e*sizeof(unsigned long));
      ge=ng->e;
      gn=ng->n;
  }
  nne=0;
  t1=time(NULL);
  printf("- Time to load the graph = %ldh%ldm%lds\n",(t1-t0)/3600,((t1-t0)%3600)/60,((t1-t0)%60));
  unsigned long  *v=(unsigned long*)malloc(ng->n*sizeof(unsigned long));
  unsigned long *t=(unsigned long*)malloc(ng->n*sizeof(unsigned long));

  for (unsigned long i=0;i<ng->n;++i)
	v[i]=0;
  v[0]=1;
  unsigned long cpos=0;
 unsigned long pos=0;
t[pos++]=0;
 while (cpos<pos){
	unsigned long x=t[cpos++];
	for (unsigned long j=ng->cd[x];j<ng->cd[x+1];++j){
		unsigned long z=ng->adj[j];
		if (v[z]==0){
			t[pos++]=z;
			v[z]=1;
		}	
	}
}
printf("visited %lu nodes \n",pos);
  printf("Starting recursive bisections\n");
  printf("Prints result in file %s\n",argv[2]);
  FILE* file=fopen(argv[2],"w");
  unsigned long k=atoi(argv[3]);
  printf("k is %lu \n",k);

  recurs(ng, 0, file,k);
  fclose(file);
	
  t2=time(NULL);
  printf("- Time to compute the hierarchy = %ldh%ldm%lds\n",(t2-t1)/3600,((t2-t1)%3600)/60,((t2-t1)%60));
  printf("- Overall time = %ldh%ldm%lds\n",(t2-t0)/3600,((t2-t0)%3600)/60,((t2-t0)%60));
  printf("\n");
  printf("space bit: %.2f | space-cut: %.2f \n",spacebit,spacecut);
  double compr1= spacebit + 128*gn + 128*spacecut ;
  double compr2=128*ge + 128*gn;
  printf("compr1= %.2f compr2= %.2f | compr = %.2f \n",compr1,compr2,(extracomp+compr1)/compr2);
  extracomp+=spacebit + 128*gn ;
  if (iters>1)
    nextIterToFile();
  }
  return 0;
}

