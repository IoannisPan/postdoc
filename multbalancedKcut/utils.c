#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>/*for gettimeofday*/
#include "utils.h"
#include "struct.h"

//-------------Doubly Linked List Functions--------------------//
void fixNext(unsigned long v, xe *xeL){
	int hnext=xeL[v].has_next;
	unsigned long previous=xeL[v].previous;
	unsigned long next=xeL[v].next;
	xeL[v].next=v+1;
	xeL[v+1].previous=v;
	xeL[v+1].has_prev=1;
	xeL[v+1].next=next;
	if (hnext==1){
		xeL[next].previous=v+1;
		xeL[v+1].has_next=hnext;
	}else{
		xeL[v].has_next=1;
		xeL[v+1].has_next=0;
	}
}
void fixPrevious(unsigned long v, xe *xeL){
		int hprev=xeL[v].has_prev;
		unsigned long previous=xeL[v].previous;
		unsigned long next=xeL[v].next;
		xeL[v].previous=v-1;
		xeL[v-1].next=v;
		xeL[v-1].has_next=1;
		xeL[v-1].previous=previous;
		if (hprev==1){	
			xeL[previous].next=v-1;
			xeL[v-1].has_prev=1;
		}
		else{
			xeL[v].has_prev=1;
			xeL[v-1].has_prev=0;
		}
}
void fixNextPrevious(unsigned long v, xe *xeL){
			int hprev=xeL[v].has_prev;
			int hnext=xeL[v].has_next;
			unsigned long previous=xeL[v].previous;
			unsigned long next=xeL[v].next;
			//printf("%lu: %d %d %lu %lu \n",v,hprev,hnext,previous,next);
			if (hprev==0){
				if (hnext==1)
					xeL[next].has_prev=0;
			}else if  (hnext==0){
					xeL[previous].has_next=0;
			}else{
				xeL[next].previous=previous;
				xeL[previous].next=next;
			}
}

void push_up(unsigned long x,unsigned long gain, unsigned long *order,xe *xeL,unsigned long *L, unsigned long *headL,unsigned long *headR){
	unsigned long px=order[x];
	unsigned long hl=*headL;
	unsigned long hr=*headR;
	unsigned long pswitch=xeL[gain].eL;
	unsigned long le=L[pswitch];
	unsigned long fpswitch=pswitch;
	if (xeL[gain+1].unset==0){
		if (xeL[gain+1].xL > xeL[gain].eL + 1)
			fpswitch=xeL[gain+1].xL-1;
	}
	if (px!=pswitch)
			L[px]=le; 	 order[le]=px;
	if (px!=fpswitch)
		L[fpswitch]=x; order[x]=fpswitch;			
	xeL[gain+1].xL=fpswitch;
	if (xeL[gain+1].unset==1){
		xeL[gain+1].unset=0;
		xeL[gain+1].eL=xeL[gain+1].xL;
		fixNext(gain,xeL);
	}
	//
	if (xeL[gain].xL == xeL[gain].eL){
		xeL[gain].unset=1;
		fixNextPrevious(gain,xeL);
		if (hl==gain)
			*headL=gain+1;
	}else{
		xeL[gain].eL--;
	}
	if (hr==gain)
		*headR=gain+1;
} 
void push_down(unsigned long x,unsigned long gain, unsigned long *order,xe *xeL,unsigned long *L, unsigned long *headL,unsigned long *headR){
	unsigned long px=order[x];
	unsigned long hl=*headL;
	unsigned long hr=*headR;
	unsigned long pswitch=xeL[gain].xL;
	unsigned long fe=L[pswitch];
	unsigned long fpswitch=pswitch;
	if (xeL[gain-1].unset==0){
		if (xeL[gain-1].eL < xeL[gain].xL - 1)
			fpswitch=xeL[gain-1].eL+1;
	}
	if (px!=pswitch)
			L[px]=fe; 	 order[fe]=px;
	if (px!=fpswitch)
		L[fpswitch]=x; order[x]=fpswitch;			
	xeL[gain-1].eL=fpswitch;
	if (xeL[gain-1].unset==1){
		xeL[gain-1].xL=xeL[gain-1].eL;
		xeL[gain-1].unset=0;
		fixPrevious(gain,xeL);
	}
	xeL[gain].xL++;
	if (xeL[gain].xL > xeL[gain].eL){
		xeL[gain].unset=1;
		fixNextPrevious(gain,xeL);
		if (hr==gain)
			*headR=gain-1;
	}
	if (gain==hl)
		*headL=gain-1;
	
}
void delete(unsigned long  x,unsigned long gain, unsigned long *order,xe *xeL,unsigned long *L,unsigned long *headL,unsigned long *headR){
		unsigned long px=order[x];
		if (px!=xeL[gain].eL){
			L[px]=L[xeL[gain].eL];
			order[L[px]]=px;
		}
		if (xeL[gain].eL==xeL[gain].xL){
			if (*headL==gain)
				*headL=xeL[gain].next;	
			xeL[gain].unset=1;
			if (gain==*headR){
				*headR=xeL[gain].previous;
			}
			fixNextPrevious(gain,xeL);
		}else{
			xeL[gain].eL--;
		}

		
}
unsigned long  ll2ul(unsigned long offset, long long g){
	if (g>=0) return g + offset;
	long long  pg= -g;
	unsigned long upg= pg;
		//printf("ll2ul: %lu and %lld = %lu \n",offset,g,offset-upg);

	return offset - upg; 
}
void update_on_v(unsigned long offset, long long *G,adjlist *g,unsigned long v,  unsigned long *headL,unsigned long *headR, xe *xeL, unsigned long *order,unsigned long *L,unsigned long lID,unsigned long *lab,int *still){
	unsigned long h1=*headL;
	unsigned long h2=*headR;
	for (unsigned long  i=g->cd[v];i<g->cd[v+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		unsigned long  z=g->adj[i];
			if (still[z]==1){
				if (lab[z]==lID){
					push_down(z,ll2ul(offset,G[z]),order,xeL,L,&h1,&h2);
					G[z]--;
					push_down(z,ll2ul(offset,G[z]),order,xeL,L,&h1,&h2);
					G[z]--;
				}
			}
	}	
	*headL=h1; 
	*headR=h2; 

}
void update_on_pair(unsigned long offset, long long *G,adjlist *g,unsigned long v,unsigned long u,unsigned long *headvL, unsigned long *headvR,xe *xeLv, unsigned long *Lv,unsigned long *headuL, unsigned long *headuR,xe *xeLu, unsigned long *Lu,unsigned long *lab,int *still,unsigned long *order){
	unsigned long lv=lab[v];
	unsigned long lu=lab[u];
	unsigned long hv1=*headvL;
	unsigned long hv2=*headvR;
	unsigned long hu1=*headuL;
	unsigned long hu2=*headuR;
	for (unsigned long  i=g->cd[v];i<g->cd[v+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		unsigned long  z=g->adj[i];
			if (still[z]==1){
				if (order[z]!=3*offset){
					if (lab[z]==lv){
						push_up(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]++;
						push_up(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]++;
					}else if (lab[z]==lu){
						push_down(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]--;
						push_down(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]--;
					}
				}
			}
	}
	for (unsigned long  i=g->cd[u];i<g->cd[u+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		unsigned long  z=g->adj[i];
			if (still[z]==1){
				if (order[z]!=3*offset){
					if (lab[z]==lu){
						push_up(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]++;
						push_up(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]++;
					}else if (lab[z]==lv){
						push_down(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]--;
						push_down(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]--;
					}
				}
			}
	}
	*headvL=hv1;
	*headvR=hv2;	
	*headuL=hu1;
	*headuR=hu2;	
}
void update_on_pair_comp(unsigned long offset, long long *G,adjlist *g,unsigned long v,unsigned long u,unsigned long *headvL, unsigned long *headvR,xe *xeLv, unsigned long *Lv,unsigned long *headuL, unsigned long *headuR,xe *xeLu, unsigned long *Lu,unsigned long *lab,int *still,unsigned long *order){
	unsigned long lv=lab[v];
	unsigned long lu=lab[u];
	unsigned long hv1=*headvL;
	unsigned long hv2=*headvR;
	unsigned long hu1=*headuL;
	unsigned long hu2=*headuR;
	for (unsigned long  i=g->cd[v];i<g->cd[v+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		unsigned long  z=g->adj[i];
			if (still[z]==1){
				if (order[z]!=3*offset){
					if (lab[z]==lv){
						push_down(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]--;
						push_down(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]--;
					}else if (lab[z]==lu){
						push_up(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]++;
						push_up(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]++;
					}
				}
			}
	}
	for (unsigned long  i=g->cd[u];i<g->cd[u+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		unsigned long  z=g->adj[i];
			if (still[z]==1 && order[z]!=3*offset){
				if (order[z]!=3*offset){
					if (lab[z]==lu){
						push_up(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]++;
						push_up(z,ll2ul(offset,G[z]),order,xeLu,Lu,&hu1,&hu2);
						G[z]++;
					}else if (lab[z]==lv){
						push_down(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]--;
						push_down(z,ll2ul(offset,G[z]),order,xeLv,Lv,&hv1,&hv2);
						G[z]--;
					}
				}
			}
	}
	*headvL=hv1;
	*headvR=hv2;	
	*headuL=hu1;
	*headuR=hu2;	
}

void update_on_v_comp(unsigned long offset, long long *G,adjlist *g,unsigned long v, unsigned long *headL,unsigned long *headR, xe *xeL, unsigned long *order,unsigned long *L,unsigned long lID,unsigned long *lab,int *still){
unsigned long h1=*headL;
	unsigned long h2=*headR;
		for (unsigned long  i=g->cd[v];i<g->cd[v+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		unsigned long  z=g->adj[i];
			if (still[z]==1){
				if (lab[z]==lID){
					push_up(z,ll2ul(offset,G[z]),order,xeL,L,&h1,&h2);
					G[z]++;
					push_up(z,ll2ul(offset,G[z]),order,xeL,L,&h1,&h2);
					G[z]++;
				}
			}
	}	
	*headL=h1; 
	*headR=h2; 
}

void create_init_ds(unsigned long *headL,unsigned long *headR,unsigned long offset, unsigned long *dL,long long *G ,xe *xeL, unsigned long *vals, unsigned long nvals,unsigned long mG,unsigned long lID,unsigned long *order, unsigned long *L,unsigned long *lab){
	unsigned long rpos=0;
	int first=1;
	unsigned long lvp=0;
	unsigned long h=0;
	for (unsigned long i=0;i<2*offset;++i){
			if (dL[i]==0){
				xeL[i].unset=1;
			}else{
				xeL[i].unset=0;
				xeL[i].xL=rpos;
				rpos+=dL[i];
				xeL[i].eL=rpos-1;
				dL[i]=xeL[i].xL;
				if (first==0){
					xeL[lvp].next=i;
					xeL[lvp].has_next=1;
					xeL[i].previous=lvp;
					xeL[i].has_prev=1;
				}else{
					h=i;
					first=0;
					xeL[i].has_prev=0;
				}
				lvp=i;
			}
	}
	xeL[lvp].has_next=0;
	for (unsigned long i=0;i<nvals;++i){
		unsigned long v=vals[i];
		if (lab[v]==lID){
			unsigned long gv=ll2ul(offset,G[v]); 
			L[dL[gv]]=v;
			order[v]=dL[gv];
			dL[gv]++;
		}
	}
	*headR=lvp;
	*headL=h;
}

void printL(unsigned long h,xe  *xeL,unsigned long *L){
	unsigned long hh=h;
	while (1>0){
		printf("%lu ",hh);
		if (xeL[hh].has_prev==1)
			printf("prev: %lu ", xeL[hh].previous);
		if (xeL[hh].has_next==1){
			printf("next: %lu ",xeL[hh].next);
		}
		printf("| ");
		for (unsigned long j=xeL[hh].xL;j<=xeL[hh].eL;++j){
			printf("%lu ",L[j]);
		}
		printf("\n");

		if (xeL[hh].has_next)
			hh=xeL[hh].next;
		else
			break;
	}
	printf("-----------------------\n");
}
long long int binSearch(unsigned long u1,unsigned long u2,adjlist * g){
	unsigned long l=g->cd[u1];
	unsigned long r=g->cd[u1+1];
	while (l<r){
		unsigned long m = (l+r)/2;
		if (g->adj[m]==u2){
			return 1;
		}else if (g->adj[m] > u2 ){
			r=m;
		}else{
			l=m+1;
		}
	}
	return 0;
}