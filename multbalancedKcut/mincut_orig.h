#include "utils.h"
#include "struct.h"
#include "stdlib.h"
void greedyMincut(adjlist *g, unsigned long *lab1,unsigned long l0,unsigned long l1,unsigned long *vids, unsigned long nvids,int *still,unsigned long nmax);
double mincut1(unsigned long nt1, unsigned long nt2,unsigned long *ntf, adjlist *g, unsigned long *lab1,unsigned long *vids, unsigned long nvids,unsigned long id0,unsigned long id1,unsigned long nmax,int *still);
