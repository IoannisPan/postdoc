
#ifndef UTILS_H__
#define UTILS_H__

#include "struct.h"

typedef struct {
  unsigned long a;
  unsigned long b;
  long long int gain;
} gpair;

typedef struct{
	unsigned long xL;
	unsigned long eL;
	int unset;
	int has_next;
	int has_prev;
	unsigned long next;
	unsigned long previous;
} xe;
int cmpfunc (const void * a, const void * b);
unsigned long  ll2ul(unsigned long offset, long long g);
//-------------Doubly Linked List Functions--------------------//
void fixNext(unsigned long v, xe *xeL);
void fixPrevious(unsigned long v, xe *xeL);
void fixNextPrevious(unsigned long v, xe *xeL);
void push_up(unsigned long x,unsigned long gain, unsigned long *order,xe *xeL,unsigned long *L, unsigned long *headL,unsigned long *headR);
void push_down(unsigned long x,unsigned long gain, unsigned long *order,xe *xeL,unsigned long *L, unsigned long *headL,unsigned long *headR);
void delete(unsigned long  x,unsigned long gain, unsigned long *order,xe *xeL,unsigned long *L,unsigned long *headL,unsigned long *headR);
void update_on_v(unsigned long offset, long long *G,adjlist *g,unsigned long v, unsigned long *headL, unsigned long *headR,xe *xeL, unsigned long *order,unsigned long *L,unsigned long lID,unsigned long *lab,int *still);
void update_on_pair(unsigned long offset, long long *G,adjlist *g,unsigned long v,unsigned long u,unsigned long *headvL, unsigned long *headvR,xe *xeLv, unsigned long *Lv,unsigned long *headuL, unsigned long *headuR,xe *xeLu, unsigned long *Lu,unsigned long *lab,int *still,unsigned long *order);
void update_on_pair_comp(unsigned long offset, long long *G,adjlist *g,unsigned long v,unsigned long u,unsigned long *headvL, unsigned long *headvR,xe *xeLv, unsigned long *Lv,unsigned long *headuL, unsigned long *headuR,xe *xeLu, unsigned long *Lu,unsigned long *lab,int *still,unsigned long *order);
void update_on_v_comp(unsigned long offset, long long *G,adjlist *g,unsigned long v, unsigned long *headL,unsigned  long *headR, xe *xeL, unsigned long *order,unsigned long *L,unsigned long lID,unsigned long *lab,int *still);
void create_init_ds(unsigned long *headL,unsigned long *headR,unsigned long offset, unsigned long *dL,long long *G ,xe *xeL, unsigned long *vals, unsigned long nvals,unsigned long mG,unsigned long lID,unsigned long *order, unsigned long *L,unsigned long *lab);
void printL(unsigned long h,xe  *xeL,unsigned long *L);
///

long long int binSearch(unsigned long u1,unsigned long u2,adjlist * g);
#endif
