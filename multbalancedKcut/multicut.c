#include "utils.h"
#include "mincut_orig.h"
#include "mincut_comp.h"
#include <stdio.h>
unsigned long multkcut(adjlist *g, unsigned long *lab,unsigned long  k){
	unsigned long *vids=(unsigned long*)malloc(g->n*sizeof(unsigned long));
	unsigned long nvids=g->n;
	unsigned long samplesize=   g->n/k;
	
	unsigned long *lab1=(unsigned long*)malloc(g->n*sizeof(unsigned long));
	unsigned long *lab2=(unsigned long*)malloc(g->n*sizeof(unsigned long));
	unsigned long *ni=(unsigned long*)malloc(k*sizeof(unsigned long));

	unsigned long *cicj=(unsigned long*)malloc(k*k*sizeof(unsigned long));
	int *still=(int*)malloc(g->n*sizeof(int));
	for (unsigned long i=0;i<k;++i)
		ni[i]=0;
	for (unsigned long i1=0;i1<k;++i1){
		for (unsigned long i2=0;i2<k;++i2){
			if (i1<i2){
				unsigned long pos=i1*k + i2;
				cicj[pos]=0;
			}
		}
	}
	for (unsigned long i=0;i<g->n;++i){
		vids[i]=i;
		still[i]=1;
	}
	for (unsigned long icut=k-1;icut>=0;--icut){
	//	printf("~~~~Round %lu begins~~~~",icut);
		unsigned long ncuti=samplesize;
		if (ncuti * (icut+1) < nvids)
			ncuti++;
		if (icut>0){
	//	 	printf("ON NORMAL ");
		 	unsigned long ncutf1,ncutf2;
		 	double cut1=mincut1(samplesize,ncuti,&ncutf1, g, lab1,vids,nvids,icut-1,icut,g->n,still);
	//	 	printf("ON COMPLEMENT ");
		 	double cut2=mincut2(samplesize,ncuti,&ncutf2, g, lab2,vids,nvids,icut-1,icut,g->n,still);
		 			 //exit(0);
	//		printf("CUTS FOUND \n");
			 if (cut1>cut2){
		 		ni[icut]=ncutf2;
		 		for (unsigned long jj=0;jj<nvids;++jj)
		 			lab[vids[jj]]=lab2[vids[jj]];
		 	}else{
		 		ni[icut]=ncutf1;
		 		for (unsigned long jj=0;jj<nvids;++jj)
		 			lab[vids[jj]]=lab1[vids[jj]];
		 	}
		 }else{
		 	ni[icut]=nvids;
		 }
		// printf("{");
		unsigned long currval=0;
		while (currval<nvids){
			unsigned long v=vids[currval];
			if (lab[v]==icut){
		//		printf("%lu ",v);
				still[v]=0;
				if (icut<k-1){
					for (unsigned long ptr=g->cd[v];ptr<g->cd[v+1];++ptr){
						unsigned long u=g->adj[ptr];
						if (lab[u] > icut){
							unsigned long pos2=icut*k + lab[u];
							cicj[pos2]++;
						}
					}
				}
				if (nvids==0)
					break;
				unsigned long v=vids[currval];
				vids[currval]=vids[nvids-1];
				nvids--;

			}else{
				currval++;
			}
		}
		//printf("} \n"); //exit(0);
		if (icut==0)
			break;
	}
	unsigned long cut=0;
	for (unsigned long ci=0;ci<k;++ci){
		for (unsigned long cj=ci+1;cj<k;++cj){
			unsigned long pos=ci*k + cj;
			unsigned long cutcicj=cicj[pos];
			if (cutcicj > ni[ci]*ni[cj] - cutcicj){
				cutcicj=ni[ci]*ni[cj] - cutcicj;
			}
			cut+=cutcicj;
			//if (cutcicj>0)
			//	printf("(%lu,%lu): %lu \n",ci,cj,cutcicj);
		}
	}
	free(ni);
	free(cicj);
	free(vids);
	free(lab1);
	free(lab2);
	return cut;

}
