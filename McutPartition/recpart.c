#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>/*for gettimeofday*/

char mfile[2048] = {'\0'};
//int =0;
typedef struct {
  int u;
  int v;
} edge;


//global arrays
int *xids; 
int *ids; 
int *eids; //CSR representation supporting efficient deletes
int *order;  //an ordering of the vertices in the graph used to identify which vertices are in which partition
unsigned long *lab;	// label array for clustering
//int *p_order;  //it's supposed to hold the position of each element in the order array (like a map)
				 //I cannot remember why I defined it, I will keep commented for now
int K;
int comments=0;
unsigned long *cij;
unsigned long *ni;
unsigned long *vicj;

double u_seconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
    
}
void help() {
  printf("usage: ex2\n");
  printf("\t -f in filename\n");
    printf("\t -k num of clusters \n");
        printf("\t -c print splitting\n");



}
int parse_args(int argc, char *argv[]) {
  int opt;
  int mfile_provided = 0;
  int kgiven=0;
  while((opt = getopt(argc, argv, "f:k:c:")) != -1){
    switch(opt) {
    case 'f':
      strcpy(mfile, optarg);
      mfile_provided = 1;
      break;
    case 'k':
      K = atoi(optarg);
      kgiven=1;
      break;  
    case 'c':
      comments=atoi(optarg);
    }
  }
  if(!mfile_provided){
    printf("You need to provide a file!!!");
    help();
    return 0;
  }
  if (kgiven==0){
    printf("K is set to 2 automatically \n");
    K=2;
  }
  return 1;
}
void readfile(char *fname, edge **edges,int *n,int *m){
  	FILE *in_file=fopen(fname, "r");
  	int tn,tm, u,v,ei;
  	fscanf(in_file, "%d", &tn);
  	fscanf(in_file, "%d", &tm); 
  	//find space
  	*edges = (edge *)malloc(tm * sizeof(edge)); 
	  for (ei=0; ei<tm;ei++) {
    	fscanf(in_file, "%d %d",&u,&v);
    	if  (v>=tn) tn=v+1;
    	if (u>=tn) tn=u+1;
    	(*edges)[ei].u=u;
    	(*edges)[ei].v=v;
  	}
  	*n=tn;
  	*m=tm;
  	fclose(in_file);
}
 void initdegrees(int *d, edge *edges,int m){
  for (int ei=0;ei<m;++ei){
      d[edges[ei].u]++; 
      d[edges[ei].v]++;
  }
}
void create_csv(int *d,int n, int m,int *xids, int *eids, int *ids, edge *edges){
    xids[0]=0;
    for (int i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
    }
    for (int ei=0;ei<m;++ei){
      int eu=edges[ei].u;
      int ev=edges[ei].v;
      ids[xids[eu]++]=ev;
      ids[xids[ev]++]=eu;
    }
    xids[0]=0;
    for (int i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
        eids[i-1]=xids[i]-1;
    }
}
//reorder the order array based on the splits 
void reorder(int l,int r,int nlab,int *xcountc){
	int *countc=(int*)malloc((nlab)*sizeof(int));
	for (int i=0;i<nlab;++i)
		countc[i]=0;
	for (int i=l;i<=r;++i){
		countc[lab[order[i]]]++;
	}
	xcountc[0]=l;
	for (int i=1;i<=nlab;++i){
		xcountc[i]=xcountc[i-1] + countc[i-1];
		countc[i-1]=xcountc[i-1];
	}
	int w=0;
	for (int i=l;i<=r;++i){ //iterate over the vertices in the segment and store the order array in-place!
		int x=order[i]; //x is the i-th element in order 
		int 
		lx=lab[x]; // its cluster
		if (xcountc[lx] > i || xcountc[lx+1] <= i){ // x is not in the correct position! (i.e., it's position is outside where it should be!)
					w++;

			while (lab[order[countc[lx]]]==lx){ //keep looking for an element  within x's cluster positions in order that is out  of place
				countc[lx]++;
			}
			//replace  the two elements positions i with  countc[lx]
			int temp=order[countc[lx]];
			order[i]=temp;
			order[countc[lx]]=x;
			//p_order[x]=countc[lx];
			//p_order[temp]=i;
			i--; // do -1 to check the new element if ok etc...
				
		}

	}

	free(countc);
}
unsigned long min(unsigned long v1,unsigned long v2){
	if (v1 > v2) return v2;
	return v1;
}
unsigned long init_ls(int l,int r){
	unsigned long i,n= r - l  + 1;
	unsigned long nlab=(K>n)?n:K;
	//random side for each node (except first k)
	for (i=0;i<nlab;++i){
		lab[order[l+i]]=i;
	}	
	for (i=nlab;i<n;i++){

		lab[order[l+i]]=rand() % nlab;
	}

	return nlab;
}

unsigned long get_pos(unsigned long i,unsigned long j,unsigned long k){
	return i*k + j;
}
unsigned long get_pos_min(unsigned long i,unsigned long j,unsigned long k){
	return i*k + j;
}
unsigned long calc(unsigned long *cij,unsigned long *ni,int k){
	unsigned long value=0;
	int i,j;

	for ( i=0;i<k;++i){
		for ( j=i+1;j<k;++j){
			int ij=get_pos(i,j,k);
			unsigned long scij=min(cij[ij], ni[i] * ni[j] -cij[ij]);
	//			printf("(%d,%d): %d & (cij=%d) & (ni[%d]=%d) & (ni[%d]=%d) \n",i,j,scij,cij[ij],i,ni[i],j,ni[j]);
			value+=scij;
		}
	}
	return value;
}
	int split_ls(int l,int r){
		unsigned long i,j,n= r -l + 1,p,x;
		unsigned long k=(K>n)?n:K;
		int lxj,lxz, ljz,z;
		double w;
		init_ls(l,r);	
		int changes=1;
		for (i=0;i<k;++i){
			ni[i]=0;
			for (j=i+1;j<k;++j){
				cij[get_pos(i,j,k)]=0;
				cij[get_pos(j,i,k)]=0;
			}
		}
		for (i=l;i<=r;++i){
			int x=order[i];
			ni[lab[x]]++;
		}	
		for (int i=l;i<=r;++i){
			int x=order[i];
			for (j=0;j<k;++j)
				vicj[get_pos(x,j,k)]=0;
			for (int j=xids[x];j<=eids[x];++j){
				z=ids[j];
				if (lab[x]!=lab[z])
					cij[get_pos(lab[x],lab[z],k)]++;
				vicj[get_pos(x,lab[z],k)]++;		
		}			
	}
	while (changes){
		changes=0;
		for (i=l;i<=r;++i){
			double bdf=0;
			int bcf=-1;
			int x=order[i];
			unsigned long lx=lab[x];
			if (ni[lx]==1)
				continue;
			for (j=0;j<k;++j){
				if (j!=lab[x]){
					double df=0;				
					lxj=get_pos(lx,j,k);
					w=cij[lxj];
					w/=(1.0*ni[lx] * ni[j]);
				 	df -= min(w, 1-w);
				 	//
					w=cij[lxj] + vicj[get_pos(x,lx,k)]  - vicj[get_pos(x,j,k)];
					w/=(1.0*(ni[lx]-1)*(ni[j]+1));
					df += min(w, 1 - w);
					for (z=0;z<k;++z){ //iterate over clusters,  attempt to add to zth cluster, see if it improves or not!
						if (z!=j && z!=lx){
							int lxz=get_pos(lx,z,k);
							int ljz=get_pos(j,z,k);
							w=cij[lxz];
							w/=(1.0*(ni[lx] * ni[z] ));
							df -= min(w,1-w);
							//
							w=cij[lxz] - vicj[get_pos(x,z,k)];
							w/=(1.0*(ni[lx]-1)*(ni[z]));
							df += min(w, 1 - w);	
							//
							w=cij[ljz];
							w/=(1.0* ni[j] * ni[z]);
							df -= min(w,1-w);
							//
							w=cij[ljz] + vicj[get_pos(x,z,k)];
							w/=(1.0*(ni[j]+1)*(ni[z]) );
							df += min(w, 1- w);	

						}					
					}
					if (df < bdf){
								bdf=df;
								bcf=j;
								
					}	
				}	
			}
			if (bdf < 0){ //change cluster from  lab[i] --> bcf
					ni[lx]--;
					ni[bcf]++;
					for (z=xids[x];z<=eids[x];++z){

						p=ids[z];
						vicj[get_pos(p,bcf,k)]++;
						vicj[get_pos(p,lx,k)]--;
						if (lab[p]==lab[x]){ 
							cij[get_pos(bcf,lab[p],k)]++;
							cij[get_pos(lab[p],bcf,k)]++;
						}
						else if (lab[p]==bcf){
							cij[get_pos(lx,bcf,k)]--;
							cij[get_pos(bcf,lx,k)]--;
						}else{
							cij[get_pos(lx,lab[p],k)]--;
							cij[get_pos(lab[p],lx,k)]--;
							cij[get_pos(lab[p],bcf,k)]++;
							cij[get_pos(bcf,lab[p],k)]++;
						}
					}
					lab[x]=bcf;
					changes=1;
			}		
		}
	}  


	return k;
}
void  cleanVertices(int l,int r,int *ee){
	int edges=0;
	for (int i=l;i<=r;++i){
		int x=order[i];
		int tx=eids[x];
		for (int j=xids[x];j<=tx;++j){
			int z=ids[j];
			if (lab[z]!=lab[x]){ 
			//edge is in the cut, replace with the last valid edge of x!
			//this way, we will ignore it
				ids[j]=ids[tx];
				tx--;
				j--;
			}
		}
		eids[x]=tx; //replace eids with its new & correct value
		ee[lab[x]]+= tx - xids[x] +1;
	}
}
void print_adj_list(int l,int r){
	for (int i=l;i<=r;++i){
		int x=order[i];
		printf("%d [%d,%d]: ",x,xids[x],eids[x]);
		for (int j=xids[x];j<=eids[x];++j){
			printf("%d ",ids[j]);
		}
		printf("\n");
	}
}
void recsplit(int l,int r,int comments){	
	if (comments==1){
		printf("On cluster {");
	for (int i=l;i<r;++i)
		printf("%d, ",order[i]);
	printf("%d}",order[r]);
	printf("\n"); 
    }
	int nlab=split_ls(l,r);
	int *xcountc=(int*)malloc((1+nlab)*sizeof(int));
	int *ee=(int*)malloc(nlab*sizeof(int));
	reorder(l,r,nlab,xcountc); //reorder the array based on split values
		if (comments==1){

	for (int i=0;i<nlab;++i){
		printf("		{");		
		for (int j=xcountc[i];j<xcountc[i+1]-1;++j)
			printf("%d, ",order[j]);
		printf("%d} \n ",order[xcountc[i+1]-1]);
	}
	printf("\n ================ \n");
	}
	for (int i=0;i<nlab;++i) 
		ee[i]=0;
	cleanVertices(l,r,ee);  //reshape the adjacency lists!
	//print_adj_list(l,r);
	for (int i=0;i<nlab;++i){ //iterate over the clusters
		//ei/=2;
		if (ee[i]>0) //if greater than zero, recurse on it
			recsplit(xcountc[i],xcountc[i+1]-1,comments); //recursive split on the i-th cluster //note this avoids the create the graphs explicitly!
	}
	free(ee);
	free(xcountc);

}

int main(int argc, char *argv[]) {
	 if(!parse_args(argc,argv)){
    	return -1;
  	} 
  	  srand(time(NULL));
  printf("Running on %s \n",mfile);
	 int n,m;
    int *d;
    edge *edges;
    double t0= u_seconds();
        readfile(mfile,&edges,&n,&m);
    double t1= u_seconds();
    printf("Reading input  took %.2f seconds \n",t1-t0);
    //return;
    int sum=0;
    d=(int*)calloc(n,sizeof(int));
    initdegrees(d,edges,m);
    xids=(int*)malloc((n+1)*sizeof(int));
    eids=(int*)malloc((n)*sizeof(int));
    ids=(int*)malloc(2*m*sizeof(int));
    lab=(unsigned long*)malloc(n*sizeof(unsigned long));
    cij=malloc(K*K*sizeof(unsigned long));
	ni = malloc(K*sizeof(unsigned long));
	vicj=malloc(n*K*sizeof(unsigned long));
	order=(int*)malloc(n*sizeof(int));
	for (int i=0;i<n;++i) order[i]=i;   
    t0= u_seconds();
        create_csv(d,n,m,xids,eids,ids,edges);
    t1=u_seconds();  
        printf("Creating csv took %.2f seconds \n",t1-t0);

    t0= u_seconds();
    	recsplit(0,n-1,comments);
    t1=u_seconds();  
       printf("Partitioning took %.2f seconds\n",t1-t0); 
    printf("------- \n");
    free(edges);
    free(order);
    free(d);
    free(xids);
    free(ids);
    free(eids);
    free(lab);
    free(cij);
    free(vicj);
    free(ni);
}