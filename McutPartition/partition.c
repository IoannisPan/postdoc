#include "partition.h"

#define NLINKS2 8
double min(double v1,double v2){
	if (v1 > v2) return v2;
	return v1;
}
//generating n random labels (boolean values)
unsigned long init(adjlist *g,unsigned long *lab){
	unsigned long i,n=g->n;
	unsigned long nlab=(K>n)?n:K;
	
	//random side for each node (except first k)
	for (i=0;i<nlab;++i){
		lab[i]=i;
	}	
	for (i=nlab;i<n;i++){
			lab[i]=rand() % nlab;
	}

	return nlab;
}

unsigned long get_pos(unsigned long i,unsigned long j,unsigned long k){
	return i*k + j;
}
unsigned long get_pos_min(unsigned long i,unsigned long j,unsigned long k){
	return i*k + j;
}
unsigned long calc(unsigned long *cij,unsigned long *ni,int k){
	unsigned long value=0;
	int i,j;

	for ( i=0;i<k;++i){
		for ( j=i+1;j<k;++j){
			int ij=get_pos(i,j,k);
			unsigned long scij=min(cij[ij], ni[i] * ni[j] -cij[ij]);
	//			printf("(%d,%d): %d & (cij=%d) & (ni[%d]=%d) & (ni[%d]=%d) \n",i,j,scij,cij[ij],i,ni[i],j,ni[j]);
			value+=scij;
		}
	}
	return value;
}
unsigned long ls(adjlist *g,unsigned long *lab){
	unsigned long i,j,n=g->n,z,p;
	unsigned long k=(K>n)?n:K;
	int lij,liz, ljz;
	double w;
	unsigned long *labrand= malloc(n*sizeof(unsigned long));
	init(g,labrand);
	unsigned long *cij=malloc(k*k*sizeof(unsigned long));
	unsigned long *ni = malloc(k*sizeof(unsigned long));
	unsigned long *vicj=malloc(n*k*sizeof(unsigned long));
	bool changes=1;
	for (i=0;i<k;++i){
		ni[i]=0;
		for (j=i+1;j<k;++j){
			cij[get_pos(i,j,k)]=0;
			cij[get_pos(j,i,k)]=0;
		}

	}
	
	for (i=0;i<n;++i){
		ni[labrand[i]]++;
		lab[i]=labrand[i];
	//	printf("INITIALLY: lab[%d]=%d \n",i,lab[i]);
	//	printf("labrand[%d]=%d; ",i,lab[i]);
	}	

	for (int i=0;i<n;++i){
		for (j=0;j<k;++j)
			vicj[get_pos(i,j,k)]=0;
		for (int j=g->cd[i];j<g->cd[i+1];++j){
			z=g->adj[j];
			if (lab[i]!=lab[z])
				cij[get_pos(lab[i],lab[z],k)]++;
			//cij[get_pos(lab[z],lab[i],k)]++;		
			vicj[get_pos(i,lab[z],k)]++;		
		}			
	}
	
//	return k;
	int r=0;
//	unsigned long sol1=calc(cij,ni,k);
//	printf("initial solution: %u  | ",sol1);
	while (changes){
		changes=0;
		r++;
		for (i=0;i<n;++i){
				 double bdf=0;
				int bcf=-1;
				unsigned long li=lab[i];
				if (ni[li]==1)
					continue;
			for (j=0;j<k;++j){
				if (j!=lab[i]){
					double df=0;				
					lij=get_pos(li,j,k);
					w=cij[lij];
					w/=(1.0*ni[li] * ni[j]);
				 	df -= min(w, 1-w);
				 	//
					w=cij[lij] + vicj[get_pos(i,li,k)]  - vicj[get_pos(i,j,k)];
					w/=(1.0*(ni[li]-1)*(ni[j]+1));
					df += min(w, 1 - w);
					for (z=0;z<k;++z){ //iterate over clusters,  attempt to add to zth cluster, see if it improves or not!
						if (z!=j && z!=li){
							int liz=get_pos(li,z,k);
							int ljz=get_pos(j,z,k);
							w=cij[liz];
							w/=(1.0*(ni[li] * ni[z] ));
							df -= min(w,1-w);
							//
							w=cij[liz] - vicj[get_pos(i,z,k)];
							w/=(1.0*(ni[li]-1)*(ni[z]));
							df += min(w, 1 - w);	
							//
							w=cij[ljz];
							w/=(1.0* ni[j] * ni[z]);
							df -= min(w,1-w);
							//
							w=cij[ljz] + vicj[get_pos(i,z,k)];
							w/=(1.0*(ni[j]+1)*(ni[z]) );
							df += min(w, 1- w);	

						}					
					}
					if (df < bdf){
								bdf=df;
								bcf=j;
								
					//		printf("my bdf chnges to %d \n",bdf);
					}	
				}	
			}
			if (bdf < 0){ //change cluster from  lab[i] --> bcf
					//printf("changing cluster of %d from %d to %d \n",i,lab[i],bcf);
					//printf("bdf : %d \n",bdf);
					//printf("before: ");
					//calc(cij,ni,k);
					ni[li]--;
					ni[bcf]++;
					/*
					for (int z=0;z<n;++z){
						printf("%d : %d ,",z,lab[z]);
					}*/
					//printf("\n");
					for (z=g->cd[i];z<g->cd[i+1];++z){
						p=g->adj[z];
						vicj[get_pos(p,bcf,k)]++;
						vicj[get_pos(p,li,k)]--;
						if (lab[p]==lab[i]){ 
					//		printf("for edge: (%d,%d) cij[%d,%d]=%d becomes ",i,p,li,bcf,cij[get_pos(li,bcf,k)]);
							cij[get_pos(bcf,lab[p],k)]++;
							cij[get_pos(lab[p],bcf,k)]++;
							//printf("%d \n",cij[get_pos(bcf,li,k)]);
						}
						else if (lab[p]==bcf){
					//		printf("for edge: (%d,%d) cij[%d,%d]=%d becomes ",i,p,li,bcf,cij[get_pos(li,bcf,k)]);
							cij[get_pos(li,bcf,k)]--;
							cij[get_pos(bcf,li,k)]--;
		//					printf("%d \n",cij[get_pos(bcf,li,k)]);

						}else{
					//		printf("for edge: (%d,%d) cij[%d,%d]=%d becomes ",i,p,li,lab[p],cij[get_pos(li,lab[p],k)]);
							cij[get_pos(li,lab[p],k)]--;
							cij[get_pos(lab[p],li,k)]--;
					//		printf("%d \n",cij[get_pos(li,lab[p],k)]);
					//		printf("for edge: (%d,%d) cij[%d,%d]=%d becomes ",i,p,lab[p],bcf,cij[get_pos(lab[p],bcf,k)]);
							cij[get_pos(lab[p],bcf,k)]++;
							cij[get_pos(bcf,lab[p],k)]++;
					//		printf("%d \n",cij[get_pos(lab[p],bcf,k)]);


						}
					}
					//printf("after: ");
					//calc(cij,ni,k);
					lab[i]=bcf;
					//printf();
					changes=1;
					//if (i==1)
				//		exit(0);
			}		
		}
	}  


//	long sol2=calc(cij,ni,k);
//	if (sol1<sol2){
//		printf("??????????????????????????");
//		exit(0);
//	}
	//printf("final solution: %u %u \n",sol2, sol1 >= sol2);
	free(labrand);
	free(cij);
	free(vicj);
	free(ni);
	return k;
}
partition choose_partition(char *c){
	printf("Chosen partition algorithm: ");
		return ls;
	printf("unknown\n");
	exit(1);
}



