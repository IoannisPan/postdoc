#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>/*for gettimeofday*/
#include "utils.h"
char mfile[2048] = {'\0'};

int comments=0;
double u_seconds(void)
{
    struct timeval tp;    
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
}
void help() {
  printf("usage: ex2\n");
  printf("\t -f in filename\n");
    printf("\t -k num of clusters \n");
        printf("\t -c print splitting\n");
}

void KL(int *xids,int *ids,int *lab,int n){
	int n1=0;
	int n2=0;
	int changes=1;
	for (int i=0;i<n/2;++i){
			lab[i]=0;
			n1++;
	}
	for (int i=n/2;i<n;++i){
			lab[i]=1;   
			n2++;
	}//initial lab assignment (we prob. can use a heuristic for better initial assignment)
	int head1=0;
	int head2=0;	
	printf("%d and %d \n",n1,n2);
	int *G=(int*)malloc(n*sizeof(int));
	int *L1=(int*)malloc(n*sizeof(int));
	int *L2=(int*)malloc(n*sizeof(int));
	int *order=(int*)malloc(n*sizeof(int));
	xe *xeL1=(xe*)malloc((1+n)*sizeof(xe));
	xe *xeL2=(xe*)malloc((1+n)*sizeof(xe));
	int *dL1=(int*)malloc((5+n)*sizeof(int));
	int *dL2=(int*)malloc((5+n)*sizeof(int));
	gpair *gpairs=(gpair*)malloc(n/2 * sizeof(gpair));
	int l1=0;
	int l2=0;
	int rounds=0;
	while (changes){
		rounds++;
		printf("ROUND #%d \n",rounds);
		for (int i=0;i<=n;++i){
			dL1[i]=0;
			dL2[i]=0;
		}
		changes=0;
		for (int u=0;u<n;++u){
			G[u]=0;
			for (int j=xids[u];j<xids[u+1];++j){
					int v=ids[j];
					if (lab[u]!=lab[v]){
						G[u]++;
					}else{
						G[u]--;
					}
			}
			if (lab[u]==0)
				dL1[G[u]+ n1]++;
			else
				dL2[G[u]+ n2]++; //this will be used to avoid creating a BST
		}
		create_init_ds(dL1,xeL1,L1,G,order,n1,n2,lab,&head1,0); //create first ordered-list strucutre	
		create_init_ds(dL2,xeL2,L2,G,order,n2,n1,lab,&head2,1); //create second ordered-list structure	
		//print_ds(head1,xeL1);
		//print_ds(head2,xeL2);
		
		int i1=n1-1;
		int i2=n2-1;
		for (int i=0;i<n/2;++i){ //create  the pair
			int v1=head1;
			int v2=head2;
			//printf("v1 = %d (act. %d) and v2=%d  (act. %d)\n",v1,v1-n1,v2,v2-n2);
			int upper_bound=v1+v2-n;
			int lower_bound=v1+v2 -n - 2;
			int lb1=v1-1;
			int lb2=v2-1;
			int curr=0;
			int curr_u=-1;
			int curr_v=-1;
			int optfound=0;
			while (v1!=-1){
				if (v1<lb1 || optfound)  //if v1 drops below on its lower bound stop or upper bound has been met
					break;
			//	printf("L1: get inside into %d [%d,%d] \n",v1,xeL1[v1].xL,xeL1[v1].eL);

				for (int j1=xeL1[v1].xL;j1<=xeL1[v1].eL;++j1){
					if (optfound)
						break;
					int u1=L1[j1];
					int tmpv2=v2;
					while (tmpv2!=-1){
						if (tmpv2<lb2) {//if v2 drops below its lower bound stop
							break;
						}
				//	printf("%d L2: get inside into %d [%d,%d] \n",u1,v2,xeL2[v2].xL,xeL2[v2].eL);
						for (int j2=xeL2[tmpv2].xL;j2<=xeL2[tmpv2].eL;++j2){
							int u2=L2[j2];
							int p=binSearch(u1,u2,xids,ids);
							int tempg=G[u1]+G[u2] -2*p; //gain from the swap
				//			printf("my tempg is %d and curr is %d, [%d <= %d]  and (%d,%d)=%d \n", tempg,curr,lower_bound,upper_bound,u1,u2,p);
							if (curr_v==-1){   //update max. gain, if it becomes equal to the upper bound stop
								curr_u=u1;curr_v=u2; curr=tempg;
								if (curr>lower_bound) lower_bound=curr;
								if (curr==upper_bound){
									optfound=1;
									break;
								}
							}else if (tempg > curr ){
								curr_u=u1;curr_v=u2; curr=tempg;
								if (curr>lower_bound) lower_bound=curr;
								if (curr==upper_bound){
									optfound=1;
									break;
								}
							}
							if (p==0) //if meet  non-neighbor, stop because we have found the best for u1
								break;
						} 
						if (optfound)
							break;
						tmpv2=xeL2[tmpv2].previous; //all items in L2 with G = v2 have been explored, go to v2-1 (if exists)
					}
				
				}
				v1=xeL1[v1].previous;//all items in L1 with G = v1 have been explored, go to v1 -1 (if exists)
			}
			gpairs[i].a=curr_u; //save the best found
			gpairs[i].b=curr_v;
			gpairs[i].gain=curr;
			//printf("pair: %d and %d \n",curr_u,curr_v);
			if (curr_v==-1 || curr_u==-1)
				return;
		//	print_ds(head1,xeL1);
		//	print_ds(head2,xeL2);
			delete(curr_u,G[curr_u]+n1, order,xeL1,L1,&head1);
			delete(curr_v,G[curr_v]+n2, order,xeL2,L2,&head2);

		//	print_ds(head1,xeL1);
		//	print_ds(head2,xeL2);
			update_based_on_pair(curr_u,curr_v,xids,ids,xeL1,L1,xeL2,L2,G,order,n1,n2,lab,&head1,&head2);
			update_based_on_pair(curr_v,curr_u,xids,ids,xeL1,L1,xeL2,L2,G,order,n1,n2,lab,&head1,&head2);


		}
		int maxp=0;
		int maxgain=gpairs[0].gain ;
		int currgain=maxgain;
		for (int i=1;i<n/2;++i){
			currgain+=gpairs[i].gain;
			if (currgain> maxgain){
				maxp=i;
				maxgain=currgain;
			}
		}
		if (maxgain>0){
			for (int i=0;i<=maxp;++i){
				int a=gpairs[i].a;
				int b=gpairs[i].b;
				lab[a]=!lab[a];
				lab[b]=!lab[b];
				//printf("swap %d <---> %d \n",a,b);
			}
			changes=1;
		}
	}

	free(G);
	free(L1);
	free(L2);
	free(order);
	free(xeL1);
	free(xeL2);
	free(gpairs);
	free(dL1);
	free(dL2);

}

int parse_args(int argc, char *argv[]) {
  int opt;
  int mfile_provided = 0;
  int kgiven=0;
  while((opt = getopt(argc, argv, "f:c:")) != -1){
    switch(opt) {
    case 'f':
      strcpy(mfile, optarg);
      mfile_provided = 1;
      break;
    case 'c':
      comments=atoi(optarg);
    	break;	
    }
  }
  if(!mfile_provided){
    printf("You need to provide a file!!!");
    help();
    return 0;
  }

  return 1;
}

int main(int argc, char *argv[]) {
	 if(!parse_args(argc,argv)){
    	return -1;
  	} 
  	  //srand(time(NULL));
  printf("Running on %s \n",mfile);
	 int n,m;
    int *d;
    edge *edges;
    double t0= u_seconds();
        readfile(mfile,&edges,&n,&m);
    double t1= u_seconds();
    printf("Reading input  took %.2f seconds \n",t1-t0);
    //return;
    int sum=0;
    d=(int*)calloc(n,sizeof(int));
    initdegrees(d,edges,m);
    int *xids=(int*)malloc((n+1)*sizeof(int));
    int *ids=(int*)malloc(2*m*sizeof(int));
    int *lab=(int*)malloc(n*sizeof(int));

    t0= u_seconds();
        create_csv(d,n,m,xids,ids,edges);
    	free(edges);
    	free(d);
    t1=u_seconds();  
        printf("Creating csv took %.2f seconds \n",t1-t0);
    t0= u_seconds();
    	sort_csv(n,xids,ids);
    t1=u_seconds();  
    	printf("Sorting csv took %.2f seconds \n",t1-t0);
   	t0= u_seconds();
    	KL(xids,ids,lab,n);
    t1=u_seconds();  
        	printf("KL took %.2f seconds \n",t1-t0);

    printf("------- \n");
    free(xids);
    free(ids);
    free(lab);
 

}