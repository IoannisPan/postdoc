#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>/*for gettimeofday*/
#include "utils.h"

int cmpfunc (const void * a, const void * b){
   int *ea=(int*)a;
   int *eb=(int*)b;
  if (*ea > *eb){
    return 1;
  }
  return -1;
}
//-------------Doubly Linked List Functions--------------------//
void fixNext(int v, xe *xeL){
	int next=xeL[v].next;
	xeL[v].next=v+1;
	xeL[v+1].previous=v;
	xeL[v+1].next=next;
	if (next!=-1)
		xeL[next].previous=v+1;
}
void fixPrevious(int v,xe *xeL){
	int previous=xeL[v].previous;
	xeL[v].previous=v-1;
	xeL[v-1].next=v;
	xeL[v-1].previous=previous;
	if (previous!=-1)
		xeL[previous].next=v-1;
}
void fixNextPrevious(int v, xe *xeL){
			int previous=xeL[v].previous;
			int next=xeL[v].next;
			if (previous==-1){
				if (next!=-1)
					xeL[next].previous=-1;
			}else if  (next==-1){
					xeL[previous].next=-1;
			}else{
				xeL[next].previous=previous;
				xeL[previous].next=next;
			}
}
void push_up(int x,int gain, int *order,xe *xeL,int *L,int *head){
	int px=order[x];
	int h=*head;
	int pswitch=xeL[gain].eL;
	int le=L[pswitch];
	int fpswitch=pswitch;
	if (xeL[gain+1].xL > xeL[gain].eL + 1)
		fpswitch=xeL[gain+1].xL-1;
	if (px!=pswitch)
			L[px]=le; 	 order[le]=px;
	if (px!=fpswitch)
		L[fpswitch]=x; order[x]=fpswitch;			
	xeL[gain+1].xL=fpswitch;
	if (xeL[gain+1].eL==-1){
		xeL[gain+1].eL=xeL[gain+1].xL;
		fixNext(gain,xeL);
	}
	xeL[gain].eL--;
	if (xeL[gain].xL > xeL[gain].eL){
		xeL[gain].xL=-1;
		xeL[gain].eL=-1;
		fixNextPrevious(gain,xeL);
	}
	if (h==gain)
		*head=gain+1;
}
void push_down(int x,int gain, int *order,xe *xeL,int *L, int *head){
	int px=order[x];
	int h=*head;
	int pswitch=xeL[gain].xL;
	int fe=L[pswitch];
	int fpswitch=pswitch;
	if (xeL[gain-1].eL!=-1){
		if (xeL[gain-1].eL < xeL[gain].xL - 1)
			fpswitch=xeL[gain-1].eL+1;
	}
	if (px!=pswitch)
			L[px]=fe; 	 order[fe]=px;
	if (px!=fpswitch)
		L[fpswitch]=x; order[x]=fpswitch;			
	xeL[gain-1].eL=fpswitch;
	if (xeL[gain-1].xL==-1){
		xeL[gain-1].xL=xeL[gain-1].eL;
		fixPrevious(gain,xeL);
	}
	xeL[gain].xL++;
	if (xeL[gain].xL > xeL[gain].eL){
		xeL[gain].xL=-1;
		xeL[gain].eL=-1;
		fixNextPrevious(gain,xeL);
		if (gain==h)
			*head=gain-1;
	}
}
void delete(int x,int gain, int *order,xe *xeL,int *L,int *head){
		int px=order[x];
		int h=*head;
		if (px!=xeL[gain].eL){
			L[px]=L[xeL[gain].eL];
			order[L[px]]=px;
		}
		order[x]=-1;
		L[xeL[gain].eL]=-1;
		xeL[gain].eL--;
		if( xeL[gain].eL <xeL[gain].xL){
			if (h==gain)
				*head=xeL[gain].previous;	
			xeL[gain].eL=-1;
			xeL[gain].xL=-1;
			fixNextPrevious(gain,xeL);

		}
}
int binSearch(int x,int u,int *xids, int *ids){
	int p=0;
	int l=xids[u];
	int r=xids[u+1];
	while (l<r){
		int m=(l+r)/2;
		if (ids[m]==x) {
			p=1;
			break;
		}else if (ids[m]>x){
			r=m;
		}else{
			l=m+1;
		}
	}
	return p;

}
void update_based_on_pair(int u,int v,int *xids,int *ids, xe *xeL1,int *L1, xe *xeL2,int *L2,int *G,int *order,int n1,int n2,int *lab,int *head1,int *head2){
	int h1=*head1;
	int h2=*head2;
	for (int i=xids[u];i<xids[u+1];++i){ //iterate over the endpoints of u (excluding v and other set vertices, and update their score!)
		int z=ids[i];
		if (z!=v &&  order[z]!=-1){
			if (lab[z]==lab[u]){
				if (lab[z]==0){
					push_up(z,G[z]+n1,order,xeL1,L1,&h1);
				}
				else
					push_up(z,G[z]+n2,order,xeL2,L2,&h2);		
				G[z]++;
			}else{
				if (lab[z]==0){
					push_down(z,G[z]+n1,order,xeL1,L1,&h1);
				}
				else
					push_down(z,G[z]+n2,order,xeL2,L2,&h2);
				G[z]--;

			}
		}
	}
	*head1=h1; 
	*head2=h2;
}
//--------------I/O-----------------------//
void readfile(char *fname, edge **edges,int *n,int *m){
  	FILE *in_file=fopen(fname, "r");
  	int tn,tm, u,v,ei;
  	fscanf(in_file, "%d", &tn);
  	fscanf(in_file, "%d", &tm); 
  	//find space
  	*edges = (edge *)malloc(tm * sizeof(edge)); 
	  for (ei=0; ei<tm;ei++) {
    	fscanf(in_file, "%d %d",&u,&v);
    	if  (v>=tn) tn=v+1;
    	if (u>=tn) tn=u+1;
    	(*edges)[ei].u=u;
    	(*edges)[ei].v=v;
  	}
  	*n=tn;
  	*m=tm;
  	fclose(in_file);
}
//------------Adjacency List--------------------//
void create_init_ds(int *dL, xe *xeL,int *L,int *G,int *order,int na,int nb,int *lab,int *head,int l){
	int n=na+nb;
	int rpos=0;
	int lvp=-1;
	int h=0;
	for (int i_gain=-na;i_gain<=nb;++i_gain){
			int i=i_gain + na;
			if (dL[i]==0){
				xeL[i].xL=-1;
				xeL[i].eL=-1;
			}else{
				h=i;
				xeL[i].xL=rpos;
				rpos+=dL[i];
				xeL[i].eL=rpos-1;
				dL[i]=xeL[i].xL;
				if (lvp!=-1)
					xeL[lvp].next=i;
				xeL[i].previous=lvp;
				lvp=i;

			}
	}
	xeL[lvp].next=-1;
	for (int i=0;i<n;++i){
		if (lab[i]==l){
			int g=G[i] + na;
			L[dL[g]]=i;
			order[i]=dL[g];
			dL[g]++;
		}
	}
	*head=h;
}


 void initdegrees(int *d, edge *edges,int m){
  for (int ei=0;ei<m;++ei){
      d[edges[ei].u]++; 
      d[edges[ei].v]++;
  }
}
void create_csv(int *d,int n, int m,int *xids, int *ids, edge *edges){
    xids[0]=0;
    for (int i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
    }
    for (int ei=0;ei<m;++ei){
      int eu=edges[ei].u;
      int ev=edges[ei].v;
      ids[xids[eu]++]=ev;
      ids[xids[ev]++]=eu;
    }
    xids[0]=0;
    for (int i=1;i<=n;++i) {
        xids[i]=xids[i-1] + d[i-1];
    }
}
void sort_csv(int n,int *xids,int *ids){
	for (int i=0;i<n;++i){
	     int nd=xids[i+1]-xids[i];
	     qsort(&ids[xids[i]],nd,sizeof(int),cmpfunc);
		for (int j=xids[i]+1;j<xids[i+1];++j){	
				if (ids[j] < ids[j-1])
					printf("??");
		}
	}
}

void print_ds(int head,xe *xeL){
	int t=head;
	while (t!=-1){
		printf("%d: [%d,%d]",t,xeL[t].xL,xeL[t].eL);
		printf("\n");
		t=xeL[t].previous;
	}
	printf("------------\n");
}