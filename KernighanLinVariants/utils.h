typedef struct {
  int u;
  int v;
} edge;

typedef struct {
  int a;
  int b;
  int gain;
} gpair;

typedef struct{
	int xL;
	int eL;
	int next;
	int previous;
} xe;
int cmpfunc (const void * a, const void * b);
//-------------Doubly Linked List Functions--------------------//
void fixNext(int v, xe *xeL);
void fixPrevious(int v,xe *xeL);
void push_up(int x,int gain, int *order,xe *xeL,int *L,int *head);
void push_down(int x,int gain, int *order,xe *xeL,int *L, int *head);
void delete(int x,int gain, int *order,xe *xeL,int *L,int *head);
void update_based_on_pair(int u,int v,int *xids,int *ids, xe *xeL1,int *L1, xe *xeL2,int *L2,int *G,int *order,int n1,int n2,int *lab,int *head1,int *head2);
void create_init_ds(int *dL, xe *xeL,int *L,int *G,int *order,int na,int nb,int *lab,int *head,int l);
void print_ds(int head,xe *xeL);
//--------------I/O-----------------------//
void readfile(char *fname, edge **edges,int *n,int *m);
//------------Adjacency List--------------------//
void create_csv(int *d,int n, int m,int *xids, int *ids, edge *edges);
 void initdegrees(int *d, edge *edges,int m);
void sort_csv(int n,int *xids,int *ids);
int binSearch(int x,int u,int *xids, int *ids);

